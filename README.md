# README #

This README documents whatever steps are necessary to get WolfieballDraftKit application up and running.

### Details ###

* Baseball Draft kit to keep track of your fantasy baseball teams
* See functionality of app here: http://goo.gl/GTeylr
* Version 1.0

### How do I get set up? ###

* Install netbeans with JavaFX. See instructions here: https://goo.gl/H39zHg
* Create a branch to add your features

### Contribution guidelines ###

* Send a pull request once your branch is ready. I'll review and test the code before it is merged

### Who do I talk to? ###

* Repo owner: sushalpenugonda@gmail.com