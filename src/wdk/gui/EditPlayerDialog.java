/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wdk.controller.DraftEditController;
import wdk.data.Contract;
import wdk.data.Draft;
import wdk.data.Player;
import wdk.data.Team;
import static wdk.gui.WDK_GUI.CLASS_HEADING_LABEL;
import static wdk.gui.WDK_GUI.PRIMARY_STYLE_SHEET;



/**
 *
 * @author Sushal
 */
public class EditPlayerDialog extends Stage {
    GridPane gridPane;
    Scene pdScene;
    
    Label headingLabel;
    ImageView playerImageView;
    ImageView nationFlagImageView;
    Label playerNameLabel;
    Label playablePositions;
    Label FantasyTeamLabel;
    ComboBox FantasyTeamComboBox;
    Label PositionLabel;
    ComboBox PositionComboBox;
    Label ContractLabel;
    ComboBox ContractComboBox;
    Label SalaryLabel;
    TextField SalaryTextField;
    
    Button CompleteButton;
    Button CancelButton;
    Player playerPtr;
    Draft draftPtr;
    MessageDialog messageDialog;
    public EditPlayerDialog(Stage primaryStage, MessageDialog msgDialog,WDK_GUI gui){
messageDialog = msgDialog;        
// MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        //CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
         //PUT THE HEADING IN THE GRID
        headingLabel = new Label("Player Details");
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        playerImageView = new ImageView();
        
        nationFlagImageView = new ImageView();
        
        playerNameLabel = new Label();
        
        playablePositions = new Label();
        
        FantasyTeamLabel = new Label("Fantasy Team:");
        
        FantasyTeamComboBox = new ComboBox();
        
        
        
        PositionLabel = new Label("Position: ");
        PositionComboBox = new ComboBox();
        
        
        ContractLabel = new Label("Contract: ");
        ContractComboBox = new ComboBox();
        ContractComboBox.getItems().addAll("S1","S2","X");
        
        SalaryLabel = new Label("Salary($):");
        SalaryTextField = new TextField();
        
        CompleteButton = new Button("Complete");
        CancelButton = new Button("Cancel");
        
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(playerImageView, 0, 1, 1, 3);
        gridPane.add(nationFlagImageView, 1, 1, 1, 1);
        gridPane.add(playerNameLabel, 1, 2, 1, 1);
        gridPane.add(playablePositions, 1, 3, 1, 1);
        gridPane.add(FantasyTeamLabel, 0, 4, 1, 1);
        gridPane.add(FantasyTeamComboBox, 1, 4, 1, 1);
        gridPane.add(PositionLabel, 0, 5, 1, 1);
        gridPane.add(PositionComboBox, 1, 5, 1, 1);
        gridPane.add(ContractLabel, 0, 6, 1, 1);
        gridPane.add(ContractComboBox, 1, 6, 1, 1);
        gridPane.add(SalaryLabel, 0, 7, 1, 1);
        gridPane.add(SalaryTextField, 1, 7, 1, 1);
        gridPane.add(CompleteButton, 0, 8, 1, 1);
        gridPane.add(CancelButton, 1, 8, 1, 1);

        //PUT THE GRID PANE IN THE WINDOW
        pdScene = new Scene(gridPane);
        pdScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(pdScene);
        initEventHandlers(gui);
    }
    public void show(Player player, Draft draft){
        playerPtr = player;
        draftPtr = draft;
        playerImageView.setImage(player.getProfilePic());
        nationFlagImageView.setImage(player.getFlag());
        playerNameLabel.setText(player.getfName()+" "+player.getlName());
        playablePositions.setText(player.getpositions());
        SalaryTextField.setText("");
        //MAKE FUNCTION IN DRAFTS FOR QUALIFYING TEAMS LIST
        List<String> teams=new ArrayList<String>();
        for(Team i:draft.getTeams()){
                teams.add(i.getTeamName());
        }
        
        FantasyTeamComboBox.getItems().setAll(draft.getTeams());
        String[]positions;
        String posStr = player.getpositions();
        for(int i=0;i<PositionComboBox.getItems().size();i++){
           PositionComboBox.getItems().remove(i);
        }
        if(posStr.equalsIgnoreCase("p")){
             PositionComboBox.getItems().add("P");
             
        }
        else{
            positions=player.getpositions().split("_");
            PositionComboBox.getItems().addAll(positions);
        }
        
        this.showAndWait();
    }
    private void initEventHandlers(WDK_GUI gui){
        CompleteButton.setOnAction(e->{
            try{
                draftPtr.removePlayer(playerPtr);
                ObservableList<Team> Teams = draftPtr.getTeams();
                Team temp = (Team)FantasyTeamComboBox.getSelectionModel().getSelectedItem();
                playerPtr.setFTPos(PositionComboBox.getSelectionModel().getSelectedItem().toString());
                playerPtr.setContract(ContractComboBox.getSelectionModel().getSelectedItem().toString());
                playerPtr.setsalary(Double.parseDouble(SalaryTextField.getText()));
                temp.addPlayer(playerPtr);
                
                System.out.println(temp.getTeamName());
                gui.updateFTTable(temp);
                playerPtr.setfTeam(temp.getTeamName());
                playerPtr.setPickNum(draftPtr.getCount());
                gui.updateTransactionTable(playerPtr);
                gui.updateTotalPoints();
                this.hide();
            }catch(Exception x){
               messageDialog.show("Fill All the Fields");
               x.printStackTrace();
            }
            
        });
        
        CancelButton.setOnAction(e->{
            this.hide();
        });
//        FantasyTeamComboBox.setOnAction(e -> {
//            
//        });
    }   
}
