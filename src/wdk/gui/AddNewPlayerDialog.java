/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import static wdk.gui.WDK_GUI.CLASS_HEADING_LABEL;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import wdk.data.Draft;
import wdk.data.Player;
import static wdk.gui.WDK_GUI.PRIMARY_STYLE_SHEET;
//import properties_manager.PropertiesManager;
/**
 *
 * @author Sushal
 */
public class AddNewPlayerDialog extends Stage {
    //GUI CONTENTS
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label fNameLabel;
    TextField fNameTextField;
    Label lNameLabel;
    TextField lNameTextField;
    Label proTeamLabel;
    ComboBox proTeamComboBox;
    CheckBox cCheckBox;
    CheckBox _1bCheckBox;
    CheckBox _3bCheckBox;
    CheckBox _2bCheckBox;
    CheckBox ssCheckBox;
    CheckBox ofCheckBox;
    CheckBox pCheckBox;
    CheckBox[] positionCheckBoxes;
    Button completeButton;
    Button cancelButton;
           
           
    public AddNewPlayerDialog(Stage primaryStage, MessageDialog messageDialog, Draft draft){
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        //CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        //PUT THE HEADING IN THE GRID
        headingLabel = new Label("Player Details");
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        fNameLabel = new Label("First Name:");
        fNameTextField = new TextField();
        
        lNameLabel = new Label("Last Name:");
        lNameTextField = new TextField();
        
        completeButton = new Button("Complete");
        cancelButton = new Button("Cancel");
        
        proTeamLabel = new Label("Pro Team:");
        proTeamComboBox = new ComboBox();
        proTeamComboBox.getItems().addAll("ATL", "AZ", "CHC", "CIN", "COL", 
                "LAD", "MIA", "MIL", "NYM", "PHI", "PIT", "SD", "SF", "STL", 
                "WAS");
         cCheckBox= new CheckBox("C");
        _1bCheckBox= new CheckBox("1B");
        _3bCheckBox= new CheckBox("3B");
        _2bCheckBox= new CheckBox("2B");
        ssCheckBox= new CheckBox("SS");
        ofCheckBox= new CheckBox("OF");
        pCheckBox= new CheckBox("P");
       
        
        HBox hbox= new HBox();
        hbox.getChildren().addAll(
            cCheckBox,
            _1bCheckBox,
            _3bCheckBox,
            _2bCheckBox,
            ssCheckBox,
            ofCheckBox,
            pCheckBox
        );
        completeButton.setOnAction(e->{
           
        try{
            String proTeam =proTeamComboBox.getValue().toString();
            String fName = fNameTextField.getText();
            String lName = lNameTextField.getText();
            String positions= "";
            if(cCheckBox.isSelected()){
                    positions+="C_";
            }	
            if(_1bCheckBox.isSelected()){
                    positions+="1B_";
            }
            if(_3bCheckBox.isSelected()){
                    positions+="3B_";
            }
            if(_2bCheckBox.isSelected()){
                    positions+="2B_";
            }
            if(ssCheckBox.isSelected()){
                    positions+="SS_";
            }if(ofCheckBox.isSelected()){
                    positions+="OF_";
            }
            if(pCheckBox.isSelected()){
                    positions+="P";
            }
            Player player = new Player(proTeam,fName,lName,positions );
            draft.addPlayer(player);
            AddNewPlayerDialog.this.hide();
        }catch(Exception x){
           messageDialog.show("Fill All the Fields");
        }
            
            
        });
        cancelButton.setOnAction(e->{
            this.hide();
        });
        //NOW ARRANGE ALL WINDOW ELEMENTS AT ONCE
        gridPane.add(headingLabel,0,0,2,1);
        gridPane.add(fNameLabel,0,1,1,1);
        gridPane.add(fNameTextField, 1, 1,1,1);
        gridPane.add(lNameLabel,0,2,1,1);
        gridPane.add(lNameTextField,1,2,1,1);
        gridPane.add(proTeamLabel,0,3,1,1);
        gridPane.add(proTeamComboBox,1,3,1,1);
        gridPane.add(hbox,0,4,2,1);
        gridPane.add(completeButton,0,5,1,1);
        gridPane.add(cancelButton, 1, 5,1,1);
        
        //PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
        
    }
    public void show(String teamName){
        fNameTextField.setText("");
        lNameTextField.setText("");
        cCheckBox.setSelected(false);
        _1bCheckBox.setSelected(false);
        _3bCheckBox.setSelected(false);
        _2bCheckBox.setSelected(false);
        ssCheckBox.setSelected(false);
        ofCheckBox.setSelected(false);
        pCheckBox.setSelected(false);
        this.showAndWait();
        
    }
}
