/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import static wdk.gui.WDK_GUI.CLASS_HEADING_LABEL;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import wdk.data.Draft;
import wdk.data.Team;
import static wdk.gui.WDK_GUI.PRIMARY_STYLE_SHEET;
//import properties_manager.PropertiesManager;
/**
 *
 * @author Sushal
 */
public class AddTeamDialog extends Stage {
    //GUI CONTENTS
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label teamNameLabel;
    TextField teamNameTextField;
    Label teamOwnerLabel;
    TextField teamOwnerTextField;
    Button completeButton;
    Button cancelButton;
    Team teamPtr;       
    boolean editMode=false;       
    public AddTeamDialog(Stage primaryStage, MessageDialog messageDialog, Draft draft,WDK_GUI gui){
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        //CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        //PUT THE HEADING IN THE GRID
        headingLabel = new Label("Fantasy Team Details");
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        teamNameLabel = new Label("Team Name");
        teamNameTextField = new TextField();
        
        teamOwnerLabel = new Label("Team Owner");
        teamOwnerTextField = new TextField();
        
        completeButton = new Button("Complete");
        cancelButton = new Button("Cancel");
        
        completeButton.setOnAction(e->{
            if(editMode){
                teamPtr.setDetails(teamNameTextField.getText(), 
                teamOwnerTextField.getText());
                for(Team i:draft.getTeams()){
                    gui.FTeamComboBox.getItems().remove(i);
                }
                gui.FTeamComboBox.getItems().addAll(draft.getTeams());
                gui.FTeamComboBox.setValue(gui.FTeamComboBox.getItems().get(0));
                
                editMode =false;
            }
            else{
                boolean isAdded = draft.addTeam(teamNameTextField.getText(), 
                    teamOwnerTextField.getText());
                gui.FTeamComboBox.getItems().add(teamNameTextField.getText());
//                for(Team i:draft.getTeams()){
//                    gui.FTeamComboBox.getItems().remove(i);
//                }
//                gui.FTeamComboBox.getItems().addAll(draft.getTeams());
              gui.FTeamComboBox.setValue(gui.FTeamComboBox.getItems().get(gui.FTeamComboBox.getItems().size()-1));
                
            }
            AddTeamDialog.this.hide();
        });
        cancelButton.setOnAction(e->{
            this.hide();
        });
        //NOW ARRANGE ALL WINDOW ELEMENTS AT ONCE
        gridPane.add(headingLabel,0,0,2,1);
        gridPane.add(teamNameLabel,0,1,1,1);
        gridPane.add(teamNameTextField, 1, 1,1,1);
        gridPane.add(teamOwnerLabel,0,2,1,1);
        gridPane.add(teamOwnerTextField,1,2,1,1);
        gridPane.add(completeButton,0,3,1,1);
        gridPane.add(cancelButton, 1, 3,1,1);
        
        //PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
        
    }
    public void show(String teamName){
        teamNameTextField.setText(teamName);
        teamOwnerTextField.setText("");
        this.showAndWait();
        
    }

    public void showEdit(Team team) {
        if(team==null)return;
        teamPtr = team;
        teamNameTextField.setText(team.getTeamName());
        teamOwnerTextField.setText(team.getOwnerName());
        editMode = true;
        this.showAndWait();
    }
}
