package wdk.gui;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdk.WDK_PropertyType;
import wdk.data.DraftDataView;
import static wdk.WDK_StartupConstants.*;
import wdk.controller.DraftEditController;
import wdk.controller.FileController;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.Player;
import wdk.file.DraftFileManager;
import wdk.file.DraftSiteExporter;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn.CellEditEvent;
import wdk.data.Team;

/**
 *
 * @author Sushal
 */
public class WDK_GUI implements DraftDataView {

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;

    //THIS HANDLES ALL OF THE APPLICATION'S DATA
    DraftDataManager dataManager;

    //This manages draft file I/O
    DraftFileManager draftFileManager;

    //This manages exporting our site pages
    DraftSiteExporter siteExporter;

    //This handles interactions with file-related controls
    FileController fileController;

    // THIS HANDLES INTERACTIONS WITH COURSE INFO CONTROLS
    DraftEditController draftController;

    // THIS IS THE APPLICATION WINDOW
    Stage primaryStage;

    // THIS IS THE STAGE'S SCENE GRAPH
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane wdkPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newDraftButton;
    Button loadDraftButton;
    Button saveDraftButton;
    Button exportSiteButton;
    Button exitButton;

    //Tabs
    TabPane tabPane = new TabPane();
    Tab HomeScreenTab;
    Tab PlayersScreenTab;
    Tab FantasyStandingScreenTab;
    Tab DraftScreenTab;
    Tab MLBTeamsScreenTab;

    //This is The Draft Scene that contains the bottom toolbar
    //and five other scenes
    FlowPane homeFlowPane;
    FlowPane playersFlowPane;
    FlowPane fantasyStandingFlowPane;
    FlowPane draftFlowPane;
    FlowPane mlbFlowPane;

    //THESE GO IN THE HOME TAB
    Label DraftNameLabel;
    TextField DraftNameTextField;
    Button AddFTeamButton;
    Button RemoveFTeamButton;
    Button EditFTeamButton;
    public ComboBox FTeamComboBox;
    TableView<Player> TeamplayersTable;
    TableColumn ftPositionColumn;
    TableColumn ftfirstNameColumn;
    TableColumn ftlastNameColumn;
    TableColumn ftproTeamColumn;
    TableColumn ftpositionsColumn;
    TableColumn ftr_wColumn;
    TableColumn fthr_svColumn;
    TableColumn ftrbi_kColumn;
    TableColumn ftsb_eraColumn;
    TableColumn ftba_whipColumn;
    TableColumn ftestimatedValueColumn;
    TableColumn ftContractColumn;
    TableColumn ftSalaryColumn;
    
    TableView<Player> TaxiTable;
    //THESE GUYS GO IN THE PLAYERS TAB
    Button addPlayerButton;
    Button removePlayerButton;
    TextField searchPlayerTextField;
    
    ToggleGroup statsRadioGroup;
    
    TableView<Player> playersTable;
    FilteredList<Player> filteredData;
    TableColumn firstNameColumn;
    TableColumn lastNameColumn;
    TableColumn proTeamColumn;
    TableColumn positionsColumn;
    TableColumn yobColumn;
    TableColumn r_wColumn;
    TableColumn hr_svColumn;
    TableColumn rbi_kColumn;
    TableColumn sb_eraColumn;
    TableColumn ba_whipColumn;
    TableColumn estimatedValueColumn;
    TableColumn notesColumn;

    //fantasy draft screen
    Button singlePickButton;
    Button startPickButton;
    Button pausePickButton;
    TableView<Player> transactionTable;
    TableColumn pickNumTC;
    TableColumn fNameTC;
    TableColumn lNameTC;
    TableColumn fTeamTC;
    TableColumn contractTC;
    TableColumn salaryTC;

    //fantasy standings estimates
    TableView<Team> teamsEstTable;
    TableColumn teamNameEC;
    TableColumn playersNeededEC;
    TableColumn moneyLeftEC;
    TableColumn ppEC;
    TableColumn REC;
    TableColumn HREC;
    TableColumn RBIEC;
    TableColumn SBEC;
    TableColumn BAEC;
    TableColumn WEC;
    TableColumn SVEC;
    TableColumn KEC;
    TableColumn ERAEC;
    TableColumn WHIPEC;
    TableColumn TotalPointsEC;

    //MLB Tab contents
    ComboBox MLBTeamsComboBox;
    TableView<Player> MLBTable;
    FilteredList<Player> MLBfilteredData;

    // WE'LL ORGANIZE OUR WORKSPACE COMPONENTS USING A BORDER PANE
    BorderPane workspacePane;
    boolean workspaceActivated;

    //WE'LL PUT OUR WORKSPACE INSIDE A PANE
    Pane draftKitPane;

    //HERE ARE OUR DIALOGS
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    ObservableList<Player> guiPlayers;
    ObservableList<Player> teamPlayers;
    ObservableList<Player> transactionList;

    //This is the scene for fantasy teams
    public WDK_GUI(Stage initPrimaryStage) {
        primaryStage = initPrimaryStage;
    }

    //Accessor method for the data manager
    public DraftDataManager getDataManager() {
        return dataManager;
    }

    //Accessor method for the file controller
    public FileController getFileController() {
        return fileController;
    }

    //Accessor method for the file manager
    public DraftFileManager getDraftFileManager() {
        return draftFileManager;
    }

    //Accessor method for the site exporter
//    public CourseSiteExporter getSiteExporter() {
//        return siteExporter;
//    }
    /**
     * Accessor method for the window (i.e. stage).
     *
     * @return The window (i.e. Stage) used by this UI.
     */
    public Stage getWindow() {
        return primaryStage;
    }
    
    public MessageDialog getMessageDialog() {
        return messageDialog;
    }
    
    public YesNoCancelDialog getYesNoCancelDialog() {
        return yesNoCancelDialog;
    }

    //Mutator method for the data manager
    public void setDataManager(DraftDataManager initDataManager) {
        dataManager = initDataManager;
    }

    /**
     * This method fully initializes the user interface for use.
     *
     * @param windowTitle The text to appear in the UI window's title bar.
     * @param subjects The list of subjects to choose from.
     * @throws IOException Thrown if any initialization files fail to load.
     */
    public void initGUI(String windowTitle) throws IOException {
        guiPlayers = dataManager.getDraft().getPlayers(); //FXCollections.observableArrayList(dataManager.getDraft().getPlayers());
        teamPlayers = FXCollections.observableArrayList();
        transactionList = FXCollections.observableArrayList();

// INIT THE DIALOGS
        initDialogs();

        // INIT THE TOOLBAR
        initFileToolbar();

        //INIT THE TABS
        initTabs();
        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initWorkspace();

        // NOW SETUP THE EVENT HANDLERS
        initEventHandlers();

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWindow(windowTitle);
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        newDraftButton = initChildButton(fileToolbarPane, WDK_PropertyType.NEW_DRAFT_ICON, WDK_PropertyType.NEW_DRAFT_TOOLTIP, false);
        loadDraftButton = initChildButton(fileToolbarPane, WDK_PropertyType.LOAD_DRAFT_ICON, WDK_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        saveDraftButton = initChildButton(fileToolbarPane, WDK_PropertyType.SAVE_DRAFT_ICON, WDK_PropertyType.SAVE_DRAFT_TOOLTIP, true);
        exportSiteButton = initChildButton(fileToolbarPane, WDK_PropertyType.EXPORT_PAGE_ICON, WDK_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        exitButton = initChildButton(fileToolbarPane, WDK_PropertyType.EXIT_ICON, WDK_PropertyType.EXIT_TOOLTIP, false);
    }

    private void initTabs() {
        tabPane = new TabPane();
        //Does not allow any tabs to be closed
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        
        HomeScreenTab = initChildTab(tabPane, WDK_PropertyType.FANTASY_TEAMS_TAB_ICON, WDK_PropertyType.FANTASY_TEAMS_TOOLTIP);
        PlayersScreenTab = initChildTab(tabPane, WDK_PropertyType.PLAYERS_TAB_ICON, WDK_PropertyType.PLAYERS_TOOLTIP);
        FantasyStandingScreenTab = initChildTab(tabPane, WDK_PropertyType.FANTASY_STANDINGS_TAB_ICON, WDK_PropertyType.FANTASY_STANDINGS_TOOLTIP);
        DraftScreenTab = initChildTab(tabPane, WDK_PropertyType.DRAFT_TAB_ICON, WDK_PropertyType.DRAFT_TOOLTIP);
        MLBTeamsScreenTab = initChildTab(tabPane, WDK_PropertyType.MLB_TEAMS_TAB_ICON, WDK_PropertyType.MLB_TEAMS_TOOLTIP);

        //init flow pane in each tab
        homeFlowPane = initHomeTabContent(HomeScreenTab);
        playersFlowPane = initPlayersTabContent(PlayersScreenTab);
        fantasyStandingFlowPane = initFantasyTabContent(FantasyStandingScreenTab);
        draftFlowPane = initDraftTabContent(DraftScreenTab);
        mlbFlowPane = initMLBFlowPane(MLBTeamsScreenTab);
        
    }

    //ADDS CONTENTS TO HOME TAB
    private FlowPane initHomeTabContent(Tab homeTab) {
        FlowPane flowPane = new FlowPane();
        Label homeTitle = initLabel(WDK_PropertyType.HOME_SCREEN_TITLE, CLASS_SUBHEADING_LABEL);
        
        HBox FantasyTeamControls = new HBox();
        
        DraftNameLabel = initLabel(WDK_PropertyType.DRAFT_NAME_LABEL, CLASS_SUBHEADING_LABEL);
        DraftNameTextField = new TextField();
        DraftNameTextField.setPrefColumnCount(80);
        
        AddFTeamButton = initChildButton(WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_ITEM_TOOLTIP, false);
        RemoveFTeamButton = initChildButton(WDK_PropertyType.MINUS_ICON, WDK_PropertyType.REMOVE_ITEM_TOOLTIP, false);
        EditFTeamButton = initChildButton(WDK_PropertyType.EDIT_ICON, WDK_PropertyType.EDIT_DRAFT_TOOLTIP, false);
        Label SelectFTeamLabel = new Label("Select Fantasy Team:");
        DraftNameTextField = new TextField();        
        DraftNameTextField.setPrefColumnCount(10);
        
        FTeamComboBox = new ComboBox();
        FTeamComboBox.getItems().addAll(dataManager.getFTeams());
        
        FantasyTeamControls.getChildren().addAll(
                DraftNameLabel,
                DraftNameTextField,
                AddFTeamButton,
                RemoveFTeamButton,
                EditFTeamButton,
                SelectFTeamLabel,
                FTeamComboBox
        );
        
        TeamplayersTable = new TableView();
        
        ftPositionColumn = new TableColumn("Positions");
        ftfirstNameColumn = new TableColumn("First Name");
        ftlastNameColumn = new TableColumn("Last Name");
        ftproTeamColumn = new TableColumn("Pro Team");
        ftpositionsColumn = new TableColumn("Positions");
        ftr_wColumn = new TableColumn("R/W");
        fthr_svColumn = new TableColumn("HR/SV");
        ftrbi_kColumn = new TableColumn("BI/K");
        ftsb_eraColumn = new TableColumn("SB/ERA");
        ftba_whipColumn = new TableColumn("BA/WHIP");
        ftestimatedValueColumn = new TableColumn("Estimated Value");
        ftContractColumn = new TableColumn("Contract");
        ftSalaryColumn = new TableColumn("Salary");
        //LINK COLS TO DATA
        ftPositionColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("ftPos"));
        ftfirstNameColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("fName"));
        ftlastNameColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("lName"));
        ftproTeamColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("team"));
        ftpositionsColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("positions"));
        ftr_wColumn.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat1"));
        fthr_svColumn.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat2"));
        ftrbi_kColumn.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat3"));
        ftsb_eraColumn.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat4"));
        ftba_whipColumn.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat5"));
        ftestimatedValueColumn.setCellValueFactory(new PropertyValueFactory<Player, Number>("estVal"));
        ftContractColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("contract"));
        ftSalaryColumn.setCellValueFactory(new PropertyValueFactory<Player, Number>("salary"));

        //ADD COLS TO TABLE
        TeamplayersTable.getColumns().addAll(
                ftPositionColumn,
                ftfirstNameColumn,
                ftlastNameColumn,
                ftproTeamColumn,
                ftpositionsColumn,
                ftr_wColumn,
                fthr_svColumn,
                ftrbi_kColumn,
                ftsb_eraColumn,
                ftba_whipColumn,
                ftestimatedValueColumn,
                ftContractColumn,
                ftSalaryColumn
        );
        
        ftPositionColumn.setSortable(false);
        ftfirstNameColumn.setSortable(false);
        ftlastNameColumn.setSortable(false);
        ftproTeamColumn.setSortable(false);
        ftpositionsColumn.setSortable(false);
        ftr_wColumn.setSortable(false);
        fthr_svColumn.setSortable(false);
        ftrbi_kColumn.setSortable(false);
        ftsb_eraColumn.setSortable(false);
        ftba_whipColumn.setSortable(false);
        ftestimatedValueColumn.setSortable(false);
        ftContractColumn.setSortable(false);
        ftSalaryColumn.setSortable(false);

        //TeamplayersTable.setItems();
        //Init taxi table
        TaxiTable = new TableView();
        TableColumn ftPositionTC = new TableColumn("Positions");
        TableColumn ftfirstNameTC = new TableColumn("First Name");
        TableColumn ftlastNameTC = new TableColumn("Last Name");
        TableColumn ftproTeamTC = new TableColumn("Pro Team");
        TableColumn ftpositionsTC = new TableColumn("Positions");
        TableColumn ftr_wTC = new TableColumn("R/W");
        TableColumn fthr_svTC = new TableColumn("HR/SV");
        TableColumn ftrbi_kTC = new TableColumn("BI/K");
        TableColumn ftsb_eraTC = new TableColumn("SB/ERA");
        TableColumn ftba_whipTC = new TableColumn("BA/WHIP");
        TableColumn ftestimatedValueTC = new TableColumn("Estimated Value");
        TableColumn ftContractTC = new TableColumn("Contract");
        TableColumn ftSalaryTC = new TableColumn("Salary");
        //LINK COLS TO DATA
        ftPositionTC.setCellValueFactory(new PropertyValueFactory<Player, String>("ftPos"));
        ftfirstNameTC.setCellValueFactory(new PropertyValueFactory<Player, String>("fName"));
        ftlastNameTC.setCellValueFactory(new PropertyValueFactory<Player, String>("lName"));
        ftproTeamTC.setCellValueFactory(new PropertyValueFactory<Player, String>("team"));
        ftpositionsTC.setCellValueFactory(new PropertyValueFactory<Player, String>("positions"));
        ftr_wTC.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat1"));
        fthr_svTC.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat2"));
        ftrbi_kTC.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat3"));
        ftsb_eraTC.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat4"));
        ftba_whipTC.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat5"));
        ftestimatedValueTC.setCellValueFactory(new PropertyValueFactory<Player, Number>("estVal"));
        ftContractTC.setCellValueFactory(new PropertyValueFactory<Player, String>("contract"));
        ftSalaryTC.setCellValueFactory(new PropertyValueFactory<Player, Number>("salary"));

        //ADD COLS TO TABLE
        TaxiTable.getColumns().addAll(
                ftPositionTC,
                ftfirstNameTC,
                ftlastNameTC,
                ftproTeamTC,
                ftpositionsTC,
                ftr_wTC,
                fthr_svTC,
                ftrbi_kTC,
                ftsb_eraTC,
                ftba_whipTC,
                ftestimatedValueTC,
                ftContractTC,
                ftSalaryTC
        );
        
        ftPositionTC.setSortable(false);
        ftfirstNameTC.setSortable(false);
        ftlastNameTC.setSortable(false);
        ftproTeamTC.setSortable(false);
        ftpositionsTC.setSortable(false);
        ftr_wTC.setSortable(false);
        fthr_svTC.setSortable(false);
        ftrbi_kTC.setSortable(false);
        ftsb_eraTC.setSortable(false);
        ftba_whipTC.setSortable(false);
        ftestimatedValueTC.setSortable(false);
        ftContractTC.setSortable(false);
        ftSalaryTC.setSortable(false);
        
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(15, 12, 15, 12));
        vbox.getChildren().addAll(
                homeTitle,
                FantasyTeamControls,
                TeamplayersTable,
                TaxiTable
        );
        
        flowPane.getChildren().add(vbox);
        homeTab.setContent(flowPane);
        
        return flowPane;
    }

    //ADDS CONTENTS TO PLAYERS TAB
    private FlowPane initPlayersTabContent(Tab playersTab) {
        FlowPane flowPane = new FlowPane();
        
        Label playerTitle = initLabel(WDK_PropertyType.PLAYER_SCREEN_TITLE, CLASS_SUBHEADING_LABEL);
        HBox playerControls = new HBox();
        
        addPlayerButton = initChildButton(WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_ITEM_TOOLTIP, false);
        removePlayerButton = initChildButton(WDK_PropertyType.MINUS_ICON, WDK_PropertyType.REMOVE_ITEM_TOOLTIP, false);
        Label searchBoxLabel = initLabel(WDK_PropertyType.SEARCH_BAR_LABEL, CLASS_SUBHEADING_LABEL);
        searchPlayerTextField = new TextField();
        searchPlayerTextField.setPrefColumnCount(80);
        playerControls.getChildren().addAll(
                addPlayerButton,
                removePlayerButton,
                searchBoxLabel,
                searchPlayerTextField
        );
        
        HBox statsRadiosHBox = new HBox();
        statsRadiosHBox.setPadding(new Insets(15, 12, 15, 12));
        statsRadiosHBox.setSpacing(100);
        statsRadioGroup = new ToggleGroup();
        RadioButton allRB, cRB, _1bRB, c1RB, _3bRB, _2bRB, miRB, ssRB, ofRB, uRB, pRB;
        
        allRB = initRB(WDK_PropertyType.ALL_RB_TEXT, statsRadioGroup);
        cRB = initRB(WDK_PropertyType.C_RB_TEXT, statsRadioGroup);
        _1bRB = initRB(WDK_PropertyType._1B_RB_TEXT, statsRadioGroup);
        c1RB = initRB(WDK_PropertyType.CI_RB_TEXT, statsRadioGroup);
        _3bRB = initRB(WDK_PropertyType._3B_RB_TEXT, statsRadioGroup);
        _2bRB = initRB(WDK_PropertyType._2B_RB_TEXT, statsRadioGroup);
        miRB = initRB(WDK_PropertyType.MI_RB_TEXT, statsRadioGroup);
        ssRB = initRB(WDK_PropertyType.SS_RB_TEXT, statsRadioGroup);
        ofRB = initRB(WDK_PropertyType.OF_RB_TEXT, statsRadioGroup);
        uRB = initRB(WDK_PropertyType.U_RB_TEXT, statsRadioGroup);
        pRB = initRB(WDK_PropertyType.P_RB_TEXT, statsRadioGroup);
        
        allRB.setSelected(true);
        
        statsRadiosHBox.getChildren().addAll(
                allRB,
                cRB,
                _1bRB,
                c1RB,
                _3bRB,
                _2bRB,
                miRB,
                ssRB,
                ofRB,
                uRB,
                pRB
        );
        
        initPlayerTable();
        
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(15, 12, 15, 12));
        vbox.getChildren().addAll(
                playerTitle,
                playerControls,
                statsRadiosHBox,
                playersTable
        );
        
        flowPane.getChildren().add(vbox);
        playersTab.setContent(flowPane);
        
        return flowPane;
    }

    //ADDS CONTENTS TO FANTASY TAB
    private FlowPane initFantasyTabContent(Tab fantasyTab) {
        FlowPane flowPane = new FlowPane();
        Label fantasyTitle = initLabel(WDK_PropertyType.STANDING_SCREEN_TITLE, CLASS_SUBHEADING_LABEL);
        VBox vbox = new VBox();
        teamsEstTable = new TableView();
        
        teamNameEC = new TableColumn("TeamName");
        playersNeededEC = new TableColumn("Players Needed");
        moneyLeftEC = new TableColumn("$ Left");
        ppEC = new TableColumn("$ PP");
        REC = new TableColumn("R");
        HREC = new TableColumn("HR");
        RBIEC = new TableColumn("RBI");
        SBEC = new TableColumn("SB");
        BAEC = new TableColumn("BA");
        WEC = new TableColumn("W");
        SVEC = new TableColumn("SV");
        KEC = new TableColumn("K");
        ERAEC = new TableColumn("ERA");
        WHIPEC = new TableColumn("WHIP");
        TotalPointsEC = new TableColumn("Total Points");

        //link columns to data
        teamNameEC.setCellValueFactory(new PropertyValueFactory<Team, String>("teamName"));
        playersNeededEC.setCellValueFactory(new PropertyValueFactory<Team, Number>("playersNeeded"));
        moneyLeftEC.setCellValueFactory(new PropertyValueFactory<Team, Number>("moneyLeft"));
        ppEC.setCellValueFactory(new PropertyValueFactory<Team, Number>("pp"));
        REC.setCellValueFactory(new PropertyValueFactory<Team, Number>("r"));
        HREC.setCellValueFactory(new PropertyValueFactory<Team, Number>("hr"));
        RBIEC.setCellValueFactory(new PropertyValueFactory<Team, Number>("rbi"));
        SBEC.setCellValueFactory(new PropertyValueFactory<Team, Number>("sb"));
        BAEC.setCellValueFactory(new PropertyValueFactory<Team, Number>("ba"));
        WEC.setCellValueFactory(new PropertyValueFactory<Team, Number>("w"));
        SVEC.setCellValueFactory(new PropertyValueFactory<Team, Number>("sv"));
        KEC.setCellValueFactory(new PropertyValueFactory<Team, Number>("k"));
        ERAEC.setCellValueFactory(new PropertyValueFactory<Team, Number>("era"));
        WHIPEC.setCellValueFactory(new PropertyValueFactory<Team, Number>("whip"));
        TotalPointsEC.setCellValueFactory(new PropertyValueFactory<Team, Number>("totalPoints"));        
        
        teamsEstTable.getColumns().addAll(
                teamNameEC,
                playersNeededEC,
                moneyLeftEC,
                ppEC,
                REC,
                HREC,
                RBIEC,
                SBEC,
                BAEC,
                WEC,
                SVEC,
                KEC,
                ERAEC,
                WHIPEC,
                TotalPointsEC);
        teamsEstTable.setItems(dataManager.getDraft().getTeams());
        flowPane.getChildren().addAll(fantasyTitle, teamsEstTable);
        fantasyTab.setContent(flowPane);
        return flowPane;
    }

    //ADDS CONTENTS TO DRAFT TAB

    private FlowPane initDraftTabContent(Tab draftTab) {
        FlowPane flowPane = new FlowPane();
        Label draftTitle = initLabel(WDK_PropertyType.DRAFT_SCREEN_TITLE, CLASS_SUBHEADING_LABEL);
        HBox draftingControls = new HBox();
        singlePickButton = new Button("Single");
        startPickButton = new Button("Start");
        pausePickButton = new Button("Pause");
        draftingControls.getChildren().addAll(
                singlePickButton,
                startPickButton,
                pausePickButton);
        
        initTransactionTable();
        VBox draftTabVB = new VBox();
        draftTabVB.getChildren().addAll(draftTitle, draftingControls, transactionTable);
        flowPane.getChildren().addAll(draftTabVB);
        draftTab.setContent(flowPane);
        return flowPane;
    }

    void initTransactionTable() {
        //INIT TABLE
        transactionTable = new TableView();

        //INIT COLUMNS
        pickNumTC = new TableColumn("Pick#");
        fNameTC = new TableColumn("First");
        lNameTC = new TableColumn("Last");
        fTeamTC = new TableColumn("Team");
        contractTC = new TableColumn("Contract");
        salaryTC = new TableColumn("Salary($)");

        //LINK COLUMNS TO DATA
        pickNumTC.setCellValueFactory(new PropertyValueFactory<Player, Number>("pickNum"));
        fNameTC.setCellValueFactory(new PropertyValueFactory<Player, String>("fName"));
        lNameTC.setCellValueFactory(new PropertyValueFactory<Player, String>("lName"));
        fTeamTC.setCellValueFactory(new PropertyValueFactory<Player, String>("fTeam"));
        contractTC.setCellValueFactory(new PropertyValueFactory<Player, String>("contract"));
        salaryTC.setCellValueFactory(new PropertyValueFactory<Player, Number>("salary"));

        //ADD COLS TO TABLE
        transactionTable.getColumns().addAll(
                pickNumTC,
                fNameTC,
                lNameTC,
                fTeamTC,
                contractTC,
                salaryTC
        );
        
        transactionTable.setItems(transactionList);
    }

    //ADDS CONTENTS TO MLB TAB
    private FlowPane initMLBFlowPane(Tab mlbTab) {
        FlowPane flowPane = new FlowPane();
        Label fantasyTitle = initLabel(WDK_PropertyType.MLB_SCREEN_TITLE, CLASS_SUBHEADING_LABEL);
        VBox vbox = new VBox();
        MLBTeamsComboBox = new ComboBox();
        MLBTeamsComboBox.getItems().addAll("ATL", "AZ", "CHC", "CIN", "COL",
                "LAD", "MIA", "MIL", "NYM", "PHI", "PIT", "SD", "SF", "STL",
                "WAS");
        MLBTable = new TableView();
        TableColumn fNameCol = new TableColumn("First Name");
        TableColumn lNameCol = new TableColumn("Last Name");
        lNameCol.setSortType(TableColumn.SortType.ASCENDING);
        TableColumn positionsCol = new TableColumn("Positions");
        
        fNameCol.setCellValueFactory(new PropertyValueFactory<Player, String>("fName"));
        lNameCol.setCellValueFactory(new PropertyValueFactory<Player, String>("lName"));
        positionsCol.setCellValueFactory(new PropertyValueFactory<Player, String>("positions"));
        
        MLBTable.getColumns().addAll(fNameCol, lNameCol, positionsCol);
        MLBTable.setItems(dataManager.getDraft().getMLBPlayers());
        MLBTable.getSortOrder().add(lNameCol);
        
        vbox.getChildren().addAll(fantasyTitle, MLBTeamsComboBox, MLBTable);
        flowPane.getChildren().add(vbox);
        mlbTab.setContent(flowPane);
        return flowPane;
    }

    // INIT A LABEL AND SET IT'S STYLESHEET CLASS

    private Label initLabel(WDK_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }

    private RadioButton initRB(WDK_PropertyType RBProperty, ToggleGroup group) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String RBText = props.getProperty(RBProperty);
        RadioButton rb = new RadioButton(RBText);
        rb.setUserData(RBText);
        rb.setToggleGroup(group);
        return rb;
    }

    private Button initChildButton(WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        //toolbar.getChildren().add(button);
        return button;
    }

    private void initWorkspace() throws IOException {
         // THE WORKSPACE HAS A FEW REGIONS, THIS 
        // IS FOR BASIC COURSE EDITING CONTROLS
        //initBasicCourseInfoControls(subjects);

        // THIS IS FOR SELECTING PAGE LINKS TO INCLUDE
        //initPageSelectionControls();
        // THIS HOLDS ALL OUR WORKSPACE COMPONENTS, SO NOW WE MUST
        // ADD THE COMPONENTS WE'VE JUST INITIALIZED
        workspacePane = new BorderPane();
        //workspacePane.setTop(topWorkspacePane);
        workspacePane.setCenter(tabPane);
        workspacePane.getStyleClass().add(CLASS_BORDERED_PANE);

        // AND NOW PUT IT IN THE WORKSPACE
//        workspaceScrollPane = new ScrollPane();
//        workspaceScrollPane.setContent(workspacePane);
//        workspaceScrollPane.setFitToWidth(true);
        // NOTE THAT WE HAVE NOT PUT THE WORKSPACE INTO THE WINDOW,
        // THAT WILL BE DONE WHEN THE USER EITHER CREATES A NEW
        // COURSE OR LOADS AN EXISTING ONE FOR EDITING
        workspaceActivated = false;
    }

    // INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private Button initChildButton(Pane toolbar, WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }

    //INIT A TAB AND ADD IT TO A TAB PANE

    private Tab initChildTab(TabPane tabPane, WDK_PropertyType icon, WDK_PropertyType tooltip) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image tabImage = new Image(imagePath);
        Tab tab = new Tab();
        tab.setGraphic(new ImageView(tabImage));
        Tooltip tabTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        tab.setTooltip(tabTooltip);
        tabPane.getTabs().add(tab);
        return tab;
    }
    
    private void initWindow(String windowTitle) {
        //Set the window title
        primaryStage.setTitle(windowTitle);

        //Get The size of the screen
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        //and use it to size the window
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        wdkPane = new BorderPane();
        wdkPane.setTop(fileToolbarPane);
        primaryScene = new Scene(wdkPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }
    
    private void initEventHandlers() throws IOException {
        fileController = new FileController(messageDialog, yesNoCancelDialog, draftFileManager, siteExporter);
        //Draft temp = new Draft("temp");
        newDraftButton.setOnAction(e -> {
            fileController.handleNewDraftRequest(this);

            //this.reloadDraft(temp);
        });

        // THEN THE DRAFT EDITING CONTROLS
        draftController = new DraftEditController(primaryStage, messageDialog, dataManager.getDraft(), this);

        // TEXT FIELDS HAVE A DIFFERENT WAY OF LISTENING FOR TEXT CHANGES
        registerTextFieldController(searchPlayerTextField);

        //THE RADIO BUTTON CONTROLS
        statsRadioGroup.selectedToggleProperty().addListener(
                new ChangeListener<Toggle>() {
                    public void changed(ObservableValue<? extends Toggle> ov,
                            Toggle old_toggle, Toggle new_toggle) {
                        if (statsRadioGroup.getSelectedToggle() != null) {
                            String criteria = statsRadioGroup.getSelectedToggle().getUserData().toString();
//                System.out.println(searchPlayerTextField.getText().toString());
                            updatePlayerTable(criteria);
                            updateTableColumns(criteria);
                        }
                    }                    
                    
                });
        
        notesColumn.setOnEditCommit(
                new EventHandler<CellEditEvent<Player, String>>() {
                    @Override
                    public void handle(CellEditEvent<Player, String> t) {
                        ((Player) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())).setnotes(t.getNewValue());
                    }
                }
        );
        
        AddFTeamButton.setOnAction(e -> {
            draftController.handleAddTeamRequest(this, DraftNameTextField.getText());
            String teamName = FTeamComboBox.getSelectionModel().getSelectedItem().toString();
                //update the teams roster in home tab
                for (Team i : dataManager.getDraft().getTeams()) {
                    if (i.getTeamName().equalsIgnoreCase(teamName)) {
                        //updateFTTable(i);
                        TeamplayersTable.setItems(i.getSLPlayers());
                        TaxiTable.setItems(i.getTaxiPlayers());
                        break;
                    }
                }
            DraftNameTextField.clear();
        });
        
        addPlayerButton.setOnAction(e -> {
            draftController.handleAddPlayerRequest(this);
        });
        
        removePlayerButton.setOnAction(e -> {
            Player playerToRemove = playersTable.getSelectionModel()
                    .getSelectedItem();
            dataManager.getDraft().removePlayer(playerToRemove);
        });
        
        playersTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                Player player = playersTable.getSelectionModel()
                        .getSelectedItem();
                draftController.handleEditPlayerRequest(this, player, dataManager.getDraft());
                updateTotalPoints();
            }
        });
        
        FTeamComboBox.setOnAction(e -> {
            try {
                String teamName = FTeamComboBox.getSelectionModel().getSelectedItem().toString();
                //update the teams roster in home tab
                for (Team i : dataManager.getDraft().getTeams()) {
                    if (i.getTeamName().equalsIgnoreCase(teamName)) {
                        //updateFTTable(i);
                        TeamplayersTable.setItems(i.getSLPlayers());
                        TaxiTable.setItems(i.getTaxiPlayers());
                        break;
                    }
                }
            } catch (Exception i) {
                System.out.println("Nothing to select");
            }
            
        });
        
        RemoveFTeamButton.setOnAction(e -> {
            try {
                int teamIndex = FTeamComboBox.getSelectionModel().getSelectedIndex();
                String teamName = FTeamComboBox.getSelectionModel().getSelectedItem().toString();
                FTeamComboBox.getItems().remove(teamIndex);
                FTeamComboBox.setValue(FTeamComboBox.getItems().get(FTeamComboBox.getItems().size() - 1));
                dataManager.getDraft().removeTeam(teamName);
                //clear gui FT table
                teamPlayers.clear();
            } catch (Exception i) {
                System.out.println("nothing to delete");
            }
        });
        
        EditFTeamButton.setOnAction(e -> {
            String teamName = FTeamComboBox.getSelectionModel().getSelectedItem().toString();
            for (Team i : dataManager.getDraft().getTeams()) {
                if (i.getTeamName().equalsIgnoreCase(teamName)) {
                    draftController.handleEditTeamRequest(i);
                    break;
                }
            }
            DraftNameTextField.clear();
        });
        
        TeamplayersTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                Player player = TeamplayersTable.getSelectionModel()
                        .getSelectedItem();
                String teamName = FTeamComboBox.getSelectionModel().getSelectedItem().toString();
                Team team = dataManager.getDraft().getTeam(teamName);
                team.getSLPlayers().remove(player);
                dataManager.getDraft().getPlayers().add(player);
                team.updateStats();
                //updateFTTable(team);
            }
        });
        singlePickButton.setOnAction(e -> {
            draftController.handleAutoAddPlayer(this, dataManager.getDraft(), 1);
        });
        startPickButton.setOnAction(e -> {
            draftController.handleAutoAddPlayer(this, dataManager.getDraft(), 2);
        });
        
        MLBTeamsComboBox.setOnAction(e -> {
            String MLBTeamName = MLBTeamsComboBox.getSelectionModel().getSelectedItem().toString();
            
            MLBfilteredData.setPredicate(Player -> {
                if (MLBTeamName == null || MLBTeamName.isEmpty()) {
                    return true;
                }
                return Player.getproTeam().equalsIgnoreCase(MLBTeamName);
            });
        });
        MLBfilteredData = new FilteredList<>(dataManager.getDraft().getMLBPlayers(), p -> true);
        SortedList<Player> sortedData = new SortedList<>(MLBfilteredData);
        sortedData.comparatorProperty().bind(MLBTable.comparatorProperty());
        MLBTable.setItems(sortedData);
        
        pausePickButton.setOnAction(e->{
            pauseBool=true;
        });
        
    }

    //@TODO

    public void updateFTTable(Team temp) {
        if (temp == null) {
            return;
        }
//        for(int i=0;i<teamPlayers.size();i++){
//                teamPlayers.remove(i);
//        }
//        for(int i=0;i<temp.getSLPlayers().size();i++){
//            teamPlayers.add(temp.getSLPlayers().get(i));
//        }
        teamPlayers.setAll(temp.getSLPlayers());
    }

    private void updateTableColumns(String criteria) {
        if (criteria.equalsIgnoreCase("p")) {
            r_wColumn.setText("W");
            hr_svColumn.setText("SV");
            rbi_kColumn.setText("K");
            sb_eraColumn.setText("ERA");
            ba_whipColumn.setText("WHIP");
        } else if (criteria.equalsIgnoreCase("all")) {
            r_wColumn.setText("R/W");
            hr_svColumn.setText("HR/SV");
            rbi_kColumn.setText("RBI/K");
            sb_eraColumn.setText("SB/ERA");
            ba_whipColumn.setText("BA/WHIP");
        } else {
            
            r_wColumn.setText("R");
            hr_svColumn.setText("HR");
            rbi_kColumn.setText("RBI");
            sb_eraColumn.setText("SB");
            ba_whipColumn.setText("BA");
        }
    }

    // REGISTER THE EVENT LISTENER FOR A TEXT FIELD

    private void registerTextFieldController(TextField filterField) {
//        textField.textProperty().addListener((observable, oldValue, newValue) -> {
//            courseController.handleCourseChangeRequest(this);
//        });
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
//            updatePlayerTable(statsRadioGroup.getUserData().toString());
            filteredData.setPredicate(Player -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String searchCrit = newValue.toLowerCase();
//            System.out.println(searchCrit);
                String lowerCaseFilter = statsRadioGroup.getSelectedToggle().getUserData().toString().toLowerCase();
//            System.out.println(lowerCaseFilter);
                if (searchCrit.equals("")) {
                    if (lowerCaseFilter.equals("all")) {
                        return true;
                    } else if (Player.qualifiesPosition(lowerCaseFilter)) {
                        return true; // Filter matches position.
                    } else {
                        return false;
                    }
                } else {
                    if (Player.qualifiesPosition(lowerCaseFilter)) {
                        if (Player.getfName().toLowerCase().contains(searchCrit)) {
                            return true; // Filter matches first name.
                        } else if (Player.getlName().toLowerCase().contains(searchCrit)) {
                            return true; // Filter matches last name.
                        } else {
                            return false;
                        }
                    } else if (lowerCaseFilter.equals("all")) {
                        if (Player.getfName().toLowerCase().contains(searchCrit)) {
                            return true; // Filter matches first name.
                        } else if (Player.getlName().toLowerCase().contains(searchCrit)) {
                            return true; // Filter matches last name.
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            });
        });
        filteredData = new FilteredList<>(guiPlayers, p -> true);
        // 3. Wrap the FilteredList in a SortedList. 
        SortedList<Player> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(playersTable.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        playersTable.setItems(sortedData);
    }

    public void updateToolbarControls(boolean saved) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * When called this function puts the workspace into the window, revealing
     * the controls for editing a Course.
     */
    public void activateWorkspace() {
        if (!workspaceActivated) {
            // PUT THE WORKSPACE IN THE GUI
            wdkPane.setCenter(tabPane);
            workspaceActivated = true;
        }
    }

    /**
     * This function takes all of the data out of the draftToReload argument and
     * loads its values into the user interface controls.
     *
     * @param draftToReload The Draft whose data we'll load into the GUI.
     *
     *
     */
    @Override
    public void reloadDraft(Draft draftToReload) {
        // FIRST ACTIVATE THE WORKSPACE IF NECESSARY
        if (!workspaceActivated) {
            activateWorkspace();
        }

        // WE DON'T WANT TO RESPOND TO EVENTS FORCED BY
        // OUR INITIALIZATION SELECTIONS
        draftController.enable(false);

        /*
         // FIRST LOAD ALL THE BASIC COURSE INFO
         draftSubjectComboBox.setValue(draftToReload.getSubject());
         draftNumberTextField.setText("" + draftToReload.getNumber());
         draftSemesterComboBox.setValue(draftToReload.getSemester());
         draftYearComboBox.setValue(draftToReload.getYear());
         draftTitleTextField.setText(draftToReload.getTitle());
         instructorNameTextField.setText(draftToReload.getInstructor().getName());
         instructorURLTextField.setText(draftToReload.getInstructor().getHomepageURL());
         indexPageCheckBox.setSelected(draftToReload.hasDraftPage(DraftPage.INDEX));
         syllabusPageCheckBox.setSelected(draftToReload.hasDraftPage(DraftPage.SYLLABUS));
         schedulePageCheckBox.setSelected(draftToReload.hasDraftPage(DraftPage.SCHEDULE));
         hwsPageCheckBox.setSelected(draftToReload.hasDraftPage(DraftPage.HWS));
         projectsPageCheckBox.setSelected(draftToReload.hasDraftPage(DraftPage.PROJECTS));

         // THEN THE DATE PICKERS
         LocalDate startDate = draftToReload.getStartingMonday();
         startDatePicker.setValue(startDate);
         LocalDate endDate = draftToReload.getEndingFriday();
         endDatePicker.setValue(endDate);

         // THE LECTURE DAY CHECK BOXES
         mondayCheckBox.setSelected(draftToReload.hasLectureDay(DayOfWeek.MONDAY));
         tuesdayCheckBox.setSelected(draftToReload.hasLectureDay(DayOfWeek.TUESDAY));
         wednesdayCheckBox.setSelected(draftToReload.hasLectureDay(DayOfWeek.WEDNESDAY));
         thursdayCheckBox.setSelected(draftToReload.hasLectureDay(DayOfWeek.THURSDAY));
         fridayCheckBox.setSelected(draftToReload.hasLectureDay(DayOfWeek.FRIDAY));
         */
        // THE SCHEDULE ITEMS TABLE
        // THE LECTURES TABLE
        // THE HWS TABLE
        // NOW WE DO WANT TO RESPOND WHEN THE USER INTERACTS WITH OUR CONTROLS
        draftController.enable(true);
    }
    
    public void updateDraftInfo(Draft draft) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private void initDialogs() {
        messageDialog = new MessageDialog(primaryStage, CLOSE_BUTTON_LABEL);
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
    }
    
    private void updatePlayerTable(String criteria) {

        //show sorted table based on criteria
        filteredData.setPredicate(Player -> {
            if (criteria == null || criteria.isEmpty()) {
                return true;
            }
            
            String searchCrit = searchPlayerTextField.getText().toLowerCase();
//            System.out.println(searchCrit);
            String lowerCaseFilter = criteria.toLowerCase();
//            System.out.println(lowerCaseFilter);
            if (searchCrit.equals("")) {
                if (lowerCaseFilter.equals("all")) {
                    return true;
                } else if (Player.qualifiesPosition(lowerCaseFilter)) {
                    return true; // Filter matches position.
                } else {
                    return false;
                }
            } else {
                if (Player.qualifiesPosition(lowerCaseFilter)) {
                    if (Player.getfName().toLowerCase().contains(searchCrit)) {
                        return true; // Filter matches first name.
                    } else if (Player.getlName().toLowerCase().contains(searchCrit)) {
                        return true; // Filter matches last name.
                    } else {
                        return false;
                    }
                } else if (lowerCaseFilter.equals("all")) {
                    if (Player.getfName().toLowerCase().contains(searchCrit)) {
                        return true; // Filter matches first name.
                    } else if (Player.getlName().toLowerCase().contains(searchCrit)) {
                        return true; // Filter matches last name.
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }

//            return false; // Does not match.
        });
        
    }
    
    private void initPlayerTable() {
        //INIT TABLE
        playersTable = new TableView();
        playersTable.setEditable(true);
        //INIT COLUMNS
        firstNameColumn = new TableColumn("First");
        lastNameColumn = new TableColumn("Last");
        proTeamColumn = new TableColumn("Pro Team");
        positionsColumn = new TableColumn("Positions");
        yobColumn = new TableColumn("Year of Birth");
        r_wColumn = new TableColumn("R/W");
        hr_svColumn = new TableColumn("HR/SV");
        rbi_kColumn = new TableColumn("RBI/K");
        sb_eraColumn = new TableColumn("SB/ERA");
        ba_whipColumn = new TableColumn("BA/WHIP");
        estimatedValueColumn = new TableColumn("Estimated Value");
        notesColumn = new TableColumn("Notes");
        notesColumn.setEditable(true);
        //LINK COLUMNS TO DATA
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("fName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("lName"));
        proTeamColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("team"));
        positionsColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("positions"));
        yobColumn.setCellValueFactory(new PropertyValueFactory<Player, Number>("birthYear"));
        r_wColumn.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat1"));
        hr_svColumn.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat2"));
        rbi_kColumn.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat3"));
        sb_eraColumn.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat4"));
        ba_whipColumn.setCellValueFactory(new PropertyValueFactory<Player, Number>("stat5"));
//        estimatedValueColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("description"));
        notesColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("notes"));
        notesColumn.setCellFactory(TextFieldTableCell.forTableColumn());

        //ADD COLS TO TABLE
        playersTable.getColumns().addAll(
                firstNameColumn,
                lastNameColumn,
                proTeamColumn,
                positionsColumn,
                yobColumn,
                r_wColumn,
                hr_svColumn,
                rbi_kColumn,
                sb_eraColumn,
                ba_whipColumn,
                estimatedValueColumn,
                notesColumn
        );
        
        playersTable.setItems(guiPlayers);
    }
    
    public void updateTransactionTable(Player playerPtr) {
        transactionList.add(playerPtr);
    }
    //indicates if pause button was pressed
    private boolean pauseBool=false;
    public boolean pauseIsPressed() {
        return pauseBool;
    }

    public void resetPauseBtn() {
        pauseBool = false;
    }

   public void updateTotalPoints() {
        int MaxPoints = dataManager.getDraft().getTeams().size();
        //reset totals
        for(Team i:dataManager.getDraft().getTeams()){
            i.resetTotalPoints();
        }
        
        
        for(int i=0;i<10;i++){
            ObservableList<Double> tempdata=FXCollections.observableArrayList();
            
            for(int j=0;j<MaxPoints;j++){
                if(i==0){
                    tempdata.add((double)dataManager.getDraft().getTeams().get(j).rProperty().get());
                }
                if(i==1){
                    tempdata.add((double)dataManager.getDraft().getTeams().get(j).hrProperty().get());
                }
                if(i==2){
                    tempdata.add((double)dataManager.getDraft().getTeams().get(j).rbiProperty().get());
                }
                if(i==3){
                    tempdata.add((double)dataManager.getDraft().getTeams().get(j).sbProperty().get());
                }
                if(i==4){
                    tempdata.add((double)dataManager.getDraft().getTeams().get(j).baProperty().get());
                }
                if(i==5){
                    tempdata.add((double)dataManager.getDraft().getTeams().get(j).wProperty().get());
                }
                if(i==6){
                    tempdata.add((double)dataManager.getDraft().getTeams().get(j).svProperty().get());
                }
                if(i==7){
                    tempdata.add((double)dataManager.getDraft().getTeams().get(j).kProperty().get());
                }
                if(i==8){
                    tempdata.add((double)dataManager.getDraft().getTeams().get(j).eraProperty().get());
                }
                if(i==9){
                    tempdata.add((double)dataManager.getDraft().getTeams().get(j).whipProperty().get());
                }
            }
            double currentMax=0;
            for(int j=0;j<MaxPoints;j++){
                //get new minimum
                double min=-1;
                int index=0;
                for(int k=0;k<MaxPoints;k++){
                    if(min==-1){
                        min=tempdata.get(k);
                        index=k;
                    }
                    else if(min>tempdata.get(k)){
                        min = tempdata.get(k);
                        index=k;
                    }
                }
                
                //increment totals
                for(int k=0;k<MaxPoints;k++){
                    
                    if(tempdata.get(k)>=currentMax && tempdata.get(k)!=-1)
                        dataManager.getDraft().getTeams().get(k).incTotal();
                        
                }
                //set the index to -1
                tempdata.set(index, -1.0);
            }
        }
    }
    
}
