/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.file;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import wdk.data.Draft;
import wdk.gui.WDK_GUI;

/**
 *
 * @author Sushal
 */
public interface DraftFileManager {
   public void loadPlayers(Draft draftToPopulate,String dirPath)throws IOException;

    //public void saveDraft(Draft courseToSave,WDK_GUI gui)throws IOException;
}
