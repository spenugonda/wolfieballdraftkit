/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import wdk.data.Draft;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonValue;
import static wdk.WDK_StartupConstants.PATH_DRAFTS;
import wdk.data.Player;
import wdk.gui.WDK_GUI;

/**
 *
 * @author Sushal
 */
public class JsonDraftFileManager implements DraftFileManager {
     // JSON FILE READING AND WRITING CONSTANTS
    //FOR MORE INFO ON ABRIVIATIANS VISIT:
    //http://mlb.mlb.com/mlb/official_info/baseball_basics/abbreviations.jsp
    String JSON_PITCHER_ARRAY = "Pitchers";
    String JSON_HITTER_ARRAY = "Hitters";
    String JSON_FIRST_NAME = "FIRST_NAME";
    String JSON_LAST_NAME = "LAST_NAME";
    String JSON_TEAM = "TEAM";
    String JSON_QP="QP" ;
    String JSON_AB ="AB";
    String JSON_R  ="R";
    String JSON_H = "H";
    String JSON_HR= "HR"  ;
    String JSON_RBI="RBI";
    String JSON_SB=  "SB"   ;

    String JSON_IP =  "IP";
    String JSON_ER =  "ER";
    String JSON_W =  "W";
    String JSON_SV =  "SV";
    //String JSON_H =  "H";//ALREADY DEFINED
    String JSON_BB =  "BB";
    String JSON_K ="K";

    String JSON_NOTES  = "NOTES"   ;
    String JSON_YEAR_OF_BIRTH = "YEAR_OF_BIRTH";
    String JSON_NATION_OF_BIRTH = "NATION_OF_BIRTH";
String SLASH = "/";
    
    @Override
    public void loadPlayers(Draft draftToPopulate, String dirPath) throws IOException {
        String pitchersJsonFilePath = dirPath+JSON_PITCHER_ARRAY+".json";
        String hittersJsonFilePath = dirPath+JSON_HITTER_ARRAY+".json";
        String profilePicsFilePath = dirPath+"players/";
        String flagPicsFilePath = dirPath+"flags/";
        //LOAD THE PITCHERS JSON FILE WITH ALL DATA
        JsonObject jsonPitchersObj = loadJSONFile(pitchersJsonFilePath);
        JsonObject jsonHittersObj = loadJSONFile(hittersJsonFilePath);
        String nation;
        //LOAD THE PITCHERS
        JsonArray jsonPitchersArray = jsonPitchersObj.getJsonArray(JSON_PITCHER_ARRAY);
        for(int i=0;i<jsonPitchersArray.size();i++){
            JsonObject tempJsonPitcher = jsonPitchersArray.getJsonObject(i);
            nation = tempJsonPitcher.getString(JSON_NATION_OF_BIRTH);
            String first = tempJsonPitcher.getString(JSON_FIRST_NAME);
            String last = tempJsonPitcher.getString(JSON_LAST_NAME);
            Player tempPitcher = new Player(
                tempJsonPitcher.getString(JSON_TEAM),
                first,
                last,
                 Integer.parseInt(tempJsonPitcher.getString(JSON_YEAR_OF_BIRTH)),
                nation,
                tempJsonPitcher.getString(JSON_NOTES),"P"
                
            );
            String flagLocation = flagPicsFilePath+nation+".png";
            //System.out.println(flagLocation);
            try{
                Image flag = loadImageFile(flagLocation);
                tempPitcher.setFlag(flag);
            }catch(Exception e){
                System.out.println(nation+" flag not found in "+flagPicsFilePath);
            }
            
            
            String profileLocation = profilePicsFilePath+last+first+".jpg";
            try{
                tempPitcher.setProfilePic(loadImageFile(profileLocation));
            }catch(Exception e){
                System.out.println(profileLocation+" picture not found");
                profileLocation = profilePicsFilePath+"AAA_PhotoMissing.jpg";
                tempPitcher.setProfilePic(loadImageFile(profileLocation));
            }
            
            tempPitcher.setstat1(
                    Integer.parseInt(tempJsonPitcher.getString(JSON_W)));
            tempPitcher.setstat2(
                    Integer.parseInt(tempJsonPitcher.getString(JSON_SV)));
            
            tempPitcher.setstat3(
                    Integer.parseInt(tempJsonPitcher.getString(JSON_K)));
            
            
            double ER = Integer.parseInt(tempJsonPitcher.getString(JSON_ER));
            double IP = Double.parseDouble(tempJsonPitcher.getString(JSON_IP));
            tempPitcher.setIP(IP);
            double ERA;
            
            if(IP==0)ERA =0;
            else ERA = (ER*9.0/IP);
            
            tempPitcher.setstat4(ERA);
            
            double BB = Double.parseDouble(tempJsonPitcher.getString(JSON_BB));
            double H = Double.parseDouble(tempJsonPitcher.getString(JSON_H));
            double WHIP;
            if(IP==0)WHIP =0;
            else WHIP = (BB+H)/IP;
            tempPitcher.setstat5(WHIP);
            
            draftToPopulate.addPlayer(tempPitcher);
        }
        //LOAD HITTERS
        JsonArray jsonHittersArray = jsonHittersObj.getJsonArray(JSON_HITTER_ARRAY);
        for(int i=0;i<jsonHittersArray.size();i++){
            JsonObject tempJsonHitter = jsonHittersArray.getJsonObject(i);
            nation = tempJsonHitter.getString(JSON_NATION_OF_BIRTH);
            String first = tempJsonHitter.getString(JSON_FIRST_NAME);
            String last = tempJsonHitter.getString(JSON_LAST_NAME);
            Player tempHitter = new Player(
                tempJsonHitter.getString(JSON_TEAM),
                first,
                last,
                Integer.parseInt(tempJsonHitter.getString(JSON_YEAR_OF_BIRTH)),
                nation,
                tempJsonHitter.getString(JSON_NOTES),
                tempJsonHitter.getString(JSON_QP)
                
            );
            String flagLocation = flagPicsFilePath+nation+".png";
            try{
                Image flag = loadImageFile(flagLocation);
                tempHitter.setFlag(flag);
            }catch(Exception e){
                System.out.println(nation+" flag not found in "+flagPicsFilePath);
            }
            String profileLocation = profilePicsFilePath+last+first+".jpg";
            try{
                tempHitter.setProfilePic(loadImageFile(profileLocation));
            }catch(Exception e){
                System.out.println(profileLocation+" picture not found");
                profileLocation = profilePicsFilePath+"AAA_PhotoMissing.jpg";
                tempHitter.setProfilePic(loadImageFile(profileLocation));
            }
            
            tempHitter.setstat1(
                    Integer.parseInt(tempJsonHitter.getString(JSON_R)));
            tempHitter.setstat2(
                    Integer.parseInt(tempJsonHitter.getString(JSON_HR)));
            
            tempHitter.setstat3(
                    Integer.parseInt(tempJsonHitter.getString(JSON_RBI)));
            
            tempHitter.setstat4(
                    Integer.parseInt(tempJsonHitter.getString(JSON_SB)));
            
            //AB=at bats
            double AB = Double.parseDouble(tempJsonHitter.getString(JSON_AB));
            tempHitter.setAtBats(AB);
            //H=hits
            double H = Double.parseDouble(tempJsonHitter.getString(JSON_H));
            tempHitter.setHits(H);
            double BA;
            if(AB==0)BA =0;
            else BA = (H/AB);
            tempHitter.setstat5(BA);
            
            draftToPopulate.addPlayer(tempHitter);
        }
    }
     // LOADS A JSON FILE AS A SINGLE OBJECT AND RETURNS IT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }    
    //LOADS AN IMAGE FILE AND RETURNS A SINGLE IMAGE OBJECT
    private Image loadImageFile(String imageLocation)throws IOException{
        InputStream is = new FileInputStream(imageLocation);
        Image image = new Image(is);
        return image;
    }

    //@Override
    public void saveDraft(Draft draft,WDK_GUI gui)throws IOException {
        // BUILD THE FILE PATH
        //String draftListing = "" + courseToSave.getSubject() + courseToSave.getNumber();
        //String jsonFilePath = PATH_DRAFTS + SLASH + courseListing + JSON_EXT;
        promptToSave(gui,draft);
        // INIT THE WRITER
        
    }
    private void promptToSave(WDK_GUI gui,Draft draft)throws IOException{
        FileChooser fileChooser = new FileChooser();
        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JSON Files (*.json)", "*.json");
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setInitialDirectory(new File(PATH_DRAFTS));
       
        File file = fileChooser.showSaveDialog(gui.getWindow());
        
        if(file!=null){
            SaveFile(file,draft);
        }
    }
    public void SaveFile(File file,Draft draft)throws IOException{
        OutputStream os = new FileOutputStream(file.getAbsolutePath());
        JsonWriter jsonWriter = Json.createWriter(os);  
        /*
        // MAKE A JSON ARRAY FOR THE PLAYERS ARRAY
        JsonArray pagesJsonArray = makePlayersJsonArray(draft.getPlayers());
        
        // AND AN OBJECT FOR THE INSTRUCTOR
        JsonObject instructorJsonObject = makeInstructorJsonObject(courseToSave.getInstructor());
        
        // ONE FOR EACH OF OUR DATES
        JsonObject startingMondayJsonObject = makeLocalDateJsonObject(courseToSave.getStartingMonday());
        JsonObject endingFridayJsonObject = makeLocalDateJsonObject(courseToSave.getEndingFriday());
        
        // THE LECTURE DAYS ARRAY
        JsonArray lectureDaysJsonArray = makeLectureDaysJsonArray(courseToSave.getLectureDays());
        
        // THE SCHEDULE ITEMS ARRAY
        JsonArray scheduleItemsJsonArray = makeScheduleItemsJsonArray(courseToSave.getScheduleItems());
        
        // THE LECTURES ARRAY
        JsonArray lecturesJsonArray = makeLecturesJsonArray(courseToSave.getLectures());
        
        // THE HWS ARRAY
        JsonArray hwsJsonArray = makeHWsJsonArray(courseToSave.getAssignments());
        
        // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
        /*(JsonObject draftJsonObject = Json.createObjectBuilder()
                                    .add(JSON_SUBJECT, courseToSave.getSubject().toString())
                                    .add(JSON_NUMBER, courseToSave.getNumber())
                                    .add(JSON_TITLE, courseToSave.getTitle())
                                    .add(JSON_SEMESTER, courseToSave.getSemester().toString())
                                    .add(JSON_YEAR, courseToSave.getYear())
                                    .add(JSON_PAGES, pagesJsonArray)
                                    .add(JSON_INSTRUCTOR, instructorJsonObject)
                                    .add(JSON_STARTING_MONDAY, startingMondayJsonObject)
                                    .add(JSON_ENDING_FRIDAY, endingFridayJsonObject)
                                    .add(JSON_LECTURE_DAYS, lectureDaysJsonArray)
                                    .add(JSON_SCHEDULE_ITEMS, scheduleItemsJsonArray)
                                    .add(JSON_LECTURES, lecturesJsonArray)
                                    .add(JSON_HWS, hwsJsonArray)
                .build();
        */
        // AND SAVE EVERYTHING AT ONCE
        //jsonWriter.writeObject(draftJsonObject);
    }
/*
    private JsonArray makePlayersJsonArray(ObservableList<Player> players) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(Player p:players){
            
        }
    }
    */
}
