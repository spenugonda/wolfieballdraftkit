/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import wdk.data.Draft;
import wdk.data.Player;
import wdk.data.Team;
import wdk.gui.MessageDialog;
import wdk.gui.WDK_GUI;

/**
 *
 * @author DevilFruit
 */
public class autoAddtask implements Runnable {
    Draft draft;
    MessageDialog md;
    WDK_GUI gui;
    String[] positionsStr = {"C", "1B", "CI", "3B", "2B", "MI", "SS", "OF", "U", "P"};
    int[] positionsLimit = {2, 1, 1, 1, 1, 1, 1, 5, 1, 9};
    
    public autoAddtask(Draft d,MessageDialog m,WDK_GUI g) {
        draft=d;
        md=m;
        gui=g;
    }


    @Override
    public void run() {
        try {
        for (Team i : draft.getTeams()) {
            if (!i.isFull()) {
                Thread.sleep(1000);
                        //t=i;
                //String j = t.getEmptyPostiton();
                for (String j : positionsStr) {
                    while (!i.posIsFull(j)) {
                        Player p = draft.getPlayer(j);
                        if (p == null) {
                            md.show("no more players with position " + j + " can be found");
                        } else {
                            draft.getPlayers().remove(p);
                            //set the drafted player's details
                            p.setFTPos(j);
                            p.setfTeam(i.getTeamName());
                            p.setPickNum(draft.getCount());
                            p.setContract("S2");
                            p.setsalary(1);
                            //add player to the team
                            i.addPlayer(p);

                            //update fantasy team table
                            gui.updateFTTable(i);
                            //add entry to transaction table
                            gui.updateTransactionTable(p);
                            //break;
                        }
                    }
                }
            }} 
        }catch (InterruptedException ex) {
                Logger.getLogger(DraftEditController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

}
