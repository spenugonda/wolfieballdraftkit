package wdk.controller;

import wdk.data.Draft;
import wdk.error.ErrorHandler;
import wdk.gui.WDK_GUI;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;
import wdk.data.DraftDataManager;
import wdk.data.Player;
import wdk.data.Team;
import wdk.gui.AddNewPlayerDialog;
import wdk.gui.AddTeamDialog;
import wdk.gui.EditPlayerDialog;
import wdk.gui.MessageDialog;

/**
 * This controller class handles the responses to all course
 * editing input, including verification of data and binding of
 * entered data to the Draft object.
 * 
 * @author Richard McKenna
 */
public class DraftEditController {
    AddTeamDialog atd;
    AddNewPlayerDialog anpd;
    EditPlayerDialog epd;
    MessageDialog md;
    // WE USE THIS TO MAKE SURE OUR PROGRAMMED UPDATES OF UI
    // VALUES DON'T THEMSELVES TRIGGER EVENTS
    private boolean enabled;

    /**
     * Constructor that gets this controller ready, not much to
     * initialize as the methods for this function are sent all
     * the objects they need as arguments.
     */
    public DraftEditController(Stage initPrimaryStage, MessageDialog initMessageDialog,Draft draft,WDK_GUI gui) {
        atd = new AddTeamDialog(initPrimaryStage,initMessageDialog,draft,gui);       
        anpd = new AddNewPlayerDialog(initPrimaryStage,initMessageDialog,draft);
        epd = new EditPlayerDialog(initPrimaryStage,initMessageDialog,gui);
        md = initMessageDialog;
    }

    /**
     * This mutator method lets us enable or disable this controller.
     * 
     * @param enableSetting If false, this controller will not respond to
     * Draft editing. If true, it will.
     */
    public void enable(boolean enableSetting) {
        enabled = enableSetting;
    }

    /**
     * This controller function is called in response to the user changing
     * course details in the UI. It responds by updating the bound Draft
     * object using all the UI values, including the verification of that
     * data.
     * 
     * @param gui The user interface that requested the change.
     */
    public void handleDraftChangeRequest(WDK_GUI gui) {
        if (enabled) {
            try {
                // UPDATE THE COURSE, VERIFYING INPUT VALUES
                gui.updateDraftInfo(gui.getDataManager().getDraft());
                
                // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
                // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
                // THE SAVE BUTTON IS ENABLED
                gui.getFileController().markAsEdited(gui);
            } catch (Exception e) {
                // SOMETHING WENT WRONG
                ErrorHandler eH = ErrorHandler.getErrorHandler();
                eH.handleUpdateDraftError();
            }
        }
    }

    public void handleSaveCourseRequest(WDK_GUI aThis, Draft draft) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void handleAddTeamRequest(WDK_GUI gui,String teamName) {
        atd.show(teamName);
       
    }

    public void handleAddPlayerRequest(WDK_GUI aThis) {
        
        anpd.show();
    }

    public void handleEditPlayerRequest(WDK_GUI aThis, Player player, Draft draft) {
        epd.show(player, draft);
    }

    public void handleEditTeamRequest(Team team) {
        atd.showEdit(team);
    }

    public void handleAutoAddPlayer(WDK_GUI gui,Draft draft,int mode) {
        //array of positions to fill
        String[] positionsStr = {"C","1B","CI","3B","2B","MI","SS","OF","U","P"};
        int[] positionsLimit = {2,1,1,1,1,1,1,5,1,9};
        Team t=null;
        Player p=null;
        if(mode==1){
            for(Team i:draft.getTeams()){
                if(!i.isFull()){
                    t=i;
                    break;
                }
            }
            if(t==null){
                for(Team i:draft.getTeams()){
                    if (!i.isTaxiFull()) {
                        int index = (int) (Math.random() * draft.getPlayers().size());
                        Player tempP = draft.getPlayers().remove(index);
                        tempP.setsalary(1);
                        i.addPlayer(tempP);
                        //gui.updateTotalPoints();
                        return;
                    }
                }
            }
            if(t==null){
                md.show("All fantasy teams are full");
                return;
            }
            String j = t.getEmptyPostiton();
            //        for(String j:positionsStr){
                if(!t.posIsFull(j)){
                    p = draft.getPlayer(j);
                    if(p==null){
                        md.show("no more players with position "+j+" can be found");
                    }
                    else{
                        draft.getPlayers().remove(p);
                        //set the drafted player's details
                        p.setFTPos(j);
                        p.setfTeam(t.getTeamName());
                        p.setPickNum(draft.getCount());
                        p.setContract("S2");
                        p.setsalary(1);
                        //add player to the team
                        t.addPlayer(p);                    

                        //update fantasy team table
                        gui.updateFTTable(t);
                        //add entry to transaction table
                        gui.updateTransactionTable(p);
                        gui.updateTotalPoints();
                        //break;
                    }
                }
        }
        else if(mode==2){
            //autoAddtask task = new autoAddtask(draft, md, gui);
            //Thread thread = new Thread(task);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        for (Team i : draft.getTeams()) {
                            if(!i.isFull()){
                            for (String j : positionsStr) {
                                while (!i.posIsFull(j)) {
                                    //check if pause is pressed, if it is
                                    if(gui.pauseIsPressed()){
                                        gui.resetPauseBtn();
                                        return;
                                    }
                                    Platform.runLater(new Runnable() {

                                        @Override
                                        public void run() {
                                            Player p = draft.getPlayer(j);
                                            if (p == null) {
                                                md.show("no more players with position " + j + " can be found");
                                            } else {
                                                draft.getPlayers().remove(p);
                                                //set the drafted player's details
                                                p.setFTPos(j);
                                                p.setfTeam(i.getTeamName());
                                                p.setPickNum(draft.getCount());
                                                p.setContract("S2");
                                                p.setsalary(1);
                                                //add player to the team
                                                i.addPlayer(p);

                                                //update fantasy team table
                                                //gui.updateFTTable(i);
                                                //add entry to transaction table
                                                gui.updateTransactionTable(p);
                                                gui.updateTotalPoints();
                                            }
                                            }
                                        });
                                        //sleep for a bit
                                        Thread.sleep(1000);
                                    }
                                }
                            } else if (!i.isTaxiFull()) {
                                //if taxi is not full pick a player at random to fill in taxi 
                                while (!i.isTaxiFull()) {

                                    int index = (int) (Math.random() * draft.getPlayers().size());
                                    Player tempP = draft.getPlayers().remove(index);
                                    tempP.setsalary(1);
                                    i.addPlayer(tempP);
                                    //add entry to transaction table
                                    //gui.updateTransactionTable(tempP);

                                }
                            }
                        }
                    } catch (InterruptedException ex) {

                    }
                }

            }).start();

            // thread.start();
        }
    }

    /**
     * This controller function is called in response to the user changing
     * the start or end date for the course. It responds by verifying the 
     * change and then updating the bound Draft object.
     * 
     * @param gui The user interface that has the date controls.
     * @param mondayPicker The date control for selecting the first
     * Monday of the semester.
     * @param fridayPicker The date control for selecting the last
     * Friday of the semester.
     */
    /*
    public void handleDateSelectionRequest(WDK_GUI gui, DatePicker mondayPicker, DatePicker fridayPicker) {
        if (enabled) {
            // NOTE THAT WE WILL IGNORE THIS EVENT WHEN IT IS DURING INITIALIZATION
            if (fridayPicker.getValue() == null) {
                return;
            }

            // GET THE DATA THE DatePicker CONTROLS CURRENTLY HOLD
            LocalDate monday = mondayPicker.getValue();
            LocalDate friday = fridayPicker.getValue();
            Draft course = gui.getDataManager().getDraft();

            // IS MONDAY REALLY A MONDAY?
            if (monday.getDayOfWeek() != DayOfWeek.MONDAY) {
                // TURN THE DAY FOR THIS DATE PICKER BACK TO WHAT THE COURSE HAS
                mondayPicker.setValue(course.getStartingMonday());

                // AND NOTIFY THE USER OF THE ERROR
                ErrorHandler eH = ErrorHandler.getErrorHandler();
                eH.handleNotAMondayError();
            } // IS FRIDAY REALLY A FRIDAY?
            else if (friday.getDayOfWeek() != DayOfWeek.FRIDAY) {
                // TURN THE DAY FOR THIS DATE PICKER BACK TO WHAT THE COURSE HAS
                fridayPicker.setValue(course.getEndingFriday());

                // AND NOTIFY THE USER OF THE ERROR
                ErrorHandler eH = ErrorHandler.getErrorHandler();
                eH.handleNotAFridayError();
            } // IS THE START DATE BEFORE THE END DATE?
            else if (monday.isAfter(friday)) {
            // TURN THEM BOTH TO WHAT COURSE HAS, THIS IS A LITTLE TRICKY
                // BECAUSE WE DON'T WANT TO GET IN AN ENDLESS CYCLE HERE

                // HERE WE ONLY HAVE TO MOVE FRIDAY BACK
                if (friday.isBefore(course.getStartingMonday())) {
                    fridayPicker.setValue(course.getEndingFriday());
                } // HERE WE ONLY HAVE TO MOVE MONDAY BACK
                else {
                    mondayPicker.setValue(course.getStartingMonday());
                }

                // AND NOTIFY THE USER OF THE ERROR
                ErrorHandler eH = ErrorHandler.getErrorHandler();
                eH.handleStartDateAfterEndDate();
            } // IN THIS CASE ALL IS GOOD
            else {
                // MAKE SURE THE COURSE HAS THE CHANGES
                gui.updateDraftInfo(gui.getDataManager().getDraft());
            }
        }
    }
    */
}