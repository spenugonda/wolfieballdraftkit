package wdk.data;
/**
*A type to represent the types of player
*contracts
*
*@author Sushal Penugonda
**/
public enum Contract{
	X,
	S1,
	S2
}