package wdk.data;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;


/**
 *
 * @author Sushal Penugonda
 */
public class Player {
    private  StringProperty team;
    private  StringProperty fName;
    private StringProperty lName;
    private StringProperty positions;
    private  IntegerProperty birthYear ;
    private  StringProperty birthNation;
    private StringProperty contract;
    private  DoubleProperty salary=new SimpleDoubleProperty(-1);
    private StringProperty notes;
    private IntegerProperty pickNum = new SimpleIntegerProperty(0);
    PlayerType type;
    private Image nationFlag;
    private Image profilePicture;
    private IntegerProperty stat1=new SimpleIntegerProperty(-1);//R/W
    private IntegerProperty stat2=new SimpleIntegerProperty(-1);//HR/SV
    private IntegerProperty stat3=new SimpleIntegerProperty(-1);//RBI/K
    private DoubleProperty stat4=new SimpleDoubleProperty(-1);//SB/ERA
    private DoubleProperty stat5=new SimpleDoubleProperty(-1);//BA/WHIP
    private double atBats = 0;
    private double hits = 0;
    private double inningsPitched =0;
    private StringProperty ftPos;
    private StringProperty fTeam;
    //final IntegerProperty sessions;
    public static final String DEFAULT_VALUE = "<ENTER VALUE>";
    
    
//    public Player() {
//        team.set(DEFAULT_VALUE);
//        fName =   (DEFAULT_VALUE);
//	lName =   (DEFAULT_VALUE);
//        birthYear = 1900;
//        birthNation =   (DEFAULT_VALUE);
//        salary = 0.0;
//        
//    }
    public Player(String team, String fName,String lName,String Positions){
        this.team = new SimpleStringProperty(team);
        this.fName = new SimpleStringProperty(fName);
        this.lName = new SimpleStringProperty(lName);
        if(Positions.equalsIgnoreCase("p")){
            type = PlayerType.PITCHER;
            positions=new SimpleStringProperty("P");
        }
        else{
            type = PlayerType.HITTER;
            positions=new SimpleStringProperty(Positions);
        }
        this.notes = new SimpleStringProperty("");
        this.ftPos = new SimpleStringProperty("");
        this.contract = new SimpleStringProperty("");
        this.fTeam = new SimpleStringProperty("");
        
    }
    public Player(String team,String fName,String lName,int birthYear,
            String birthNation,String Notes, String Positions){
        this.team = new SimpleStringProperty(team);
        this.fName = new SimpleStringProperty(fName);
        this.lName = new SimpleStringProperty(lName);
        this.birthYear = new SimpleIntegerProperty(birthYear);
        this.birthNation = new SimpleStringProperty(birthNation);
        this.notes = new SimpleStringProperty(Notes);
        if(Positions.equalsIgnoreCase("p")){
            type = PlayerType.PITCHER;
            positions=new SimpleStringProperty("P");
        }
        else{
            type = PlayerType.HITTER;
            positions=new SimpleStringProperty(Positions);
        }
        this.ftPos = new SimpleStringProperty("");
        this.contract = new SimpleStringProperty("");
        this.fTeam = new SimpleStringProperty("");
    }
    public boolean qualifiesPosition(String position){
        if(type==PlayerType.PITCHER&&position.equalsIgnoreCase("p")){
            return true;
        }
        else if(type!=PlayerType.PITCHER&&position.equalsIgnoreCase("u")){
            return true;
        }
        else if(position.equalsIgnoreCase("mi")){
            if(qualifiesPosition("2b")||qualifiesPosition("ss")){
                return true;
            }
            return false;
        }
        else if(position.equalsIgnoreCase("ci")){
            if(qualifiesPosition("1b")||qualifiesPosition("3b")){
                return true;
            }
            return false;
        }
        else{
            String temp = position.toLowerCase();
            String qualpos = positions.get().toLowerCase();
            if(qualpos.contains(temp)){
                return true;
            }
            else return false;
        }
    }
    public void reset() {
        setfName(DEFAULT_VALUE);
        setlName(DEFAULT_VALUE);
    }
    
	//Accessors methods
	public String getproTeam(){
		return team.get() ;
	}
    public String getfName() {
        return fName.get() ;
    }
	public String getlName(){
		return lName.get();
	}
        public String getpositions(){
            return positions.get();
        }
	public int getbirthYear(){
		return birthYear.get();
	}
	public String getbirthNation(){
		return birthNation.get();
	}
	
	public double getsalary(){
		return salary.get();
	}
	public String getnotes(){
		return notes.get();
	}
        //RETURNS R/W STAT
	public int getstat1(){
            return stat1.get();
        }
        //RETURNS HR/SV STAT
        public int getstat2(){
            return stat2.get();
        }
        //RETURNS RBI/K STAT
        public int getstat3(){
            return stat3.get();
        }
        //RETURNS SB/ERA STAT
        public double getstat4(){
            return stat4.get();
        }
        //RETURNS BA/WHIP STAT
        public double getstat5(){
            
            return stat5.get();
        }
        
    //Mutator methods
    public void setTeam(String initTeam){
		team.set(initTeam);
	}
    public void setfName(String initfName) {
        fName.set(initfName);
    }
    public void setlName(String initlName){
            lName.set(initlName);
    }
    public void setbirthYear(int initbirthYear){
            birthYear.set(initbirthYear);
    }
    public void setpositions(String initposition){
        positions.set(initposition);
    }
    public void setbirthNation(String initbirthNation){
            birthNation.set(initbirthNation);
    }
    
    public void setsalary(double initSalary){
            salary.set(initSalary);
    }
    public void setnotes(String newNotes){
            notes.set (newNotes);
    }
    public void setstat1(int initstat1){
        stat1.set(initstat1);
    }
    public void setstat2(int initstat2){
        stat2.set(initstat2);
    }
    public void setstat3(int initstat3){
        stat3.set(initstat3);
    }
    public void setstat4(double initstat4){
        stat4.set(initstat4);
    }
    public void setstat5(double initstat5){
        stat5.set(initstat5);
    }
    public void setPickNum(int Num){
        pickNum.set(Num);
    }
    public void setfTeam(String team){
        fTeam.set(team);
    }
    public void setAtBats(double AB){
        atBats = AB;
    }
    
    
    public StringProperty teamProperty(){
        return team;
    }
    public StringProperty fNameProperty(){
        return fName;
    }
    public StringProperty lNameProperty(){
        return lName;
    }
    public StringProperty positionsProperty(){
        return positions;
    }
    public StringProperty birthNationProperty(){
        return birthNation;
    }
    public StringProperty notesProperty(){
        return notes;
    }
    public IntegerProperty birthYearProperty(){
        return birthYear;
    }
    public IntegerProperty stat1Property(){
        return stat1;
    }
    public IntegerProperty stat2Property(){
        return stat2;
    }
    public IntegerProperty stat3Property(){
        return stat3;
    }
    public DoubleProperty stat4Property(){
        return stat4;
    }
    public DoubleProperty stat5Property(){
        
        return stat5;
    }
    public IntegerProperty pickNumProperty(){
        return pickNum;
    }
    public StringProperty fTeamProperty(){
        return fTeam;
    }
    public void setFlag(Image flag){
        nationFlag = flag;
    }
    public Image getFlag(){
        return nationFlag;
    }
    public void setProfilePic(Image profilePic){
        profilePicture = profilePic;
    }
    public Image getProfilePic(){
        return profilePicture;
    }
    public void setFTPos(String position){
        ftPos.set(position);
    }
    public StringProperty ftPosProperty(){
        return ftPos;
    }
    public String getFTPos(){
        return ftPos.get();
    }
    public StringProperty contractProperty(){
        return contract;
    }
    public String getContract(){
        return contract.get();
    }
    public double getAtBats(){
        return atBats;
    }
    public void setContract(String c){
        contract.set(c);
    }
    public DoubleProperty salaryProperty(){
        return salary;
    }

    public void setHits(double H) {
        hits = H;
    }
    public double getHits(){
        return hits;
    }
    public double getIP(){
        return inningsPitched;
    }
    public void setIP(double IP){
        inningsPitched = IP;
    }
}
