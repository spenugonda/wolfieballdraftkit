package wdk.data;

import javafx.collections.ObservableList;
import wdk.file.DraftFileManager;

/**
 * This class manages a Draft, which means it knows how to
 * reset one with default values and generate useful dates.
 * 
 * @author Sushal Penugonda
 */
public class DraftDataManager {
    // THIS IS THE COURSE BEING EDITED
    Draft draft;
    
    // THIS IS THE UI, WHICH MUST BE UPDATED
    // WHENEVER OUR MODEL'S DATA CHANGES
    DraftDataView view;
    
    // THIS HELPS US LOAD THINGS FOR OUR COURSE
    //DraftFileManager fileManager;
    
    // DEFAULT INITIALIZATION VALUES FOR NEW drafts
    //static Subject  DEFAULT_COURSE_SUBJECT = Subject.CSE;
    //static int      DEFAULT_NUM = 0;
    //static String   DEFAULT_TEXT = "Unknown";
    //static Semester DEFAULT_SEMESTER = Semester.FALL;
    
    public DraftDataManager(DraftDataView initView) {
        view = initView;
        draft = new Draft("abc");
    }
    
    /**
     * Accessor method for getting the Course that this class manages.
     */
    public Draft getDraft() {
        return draft;
    }
    
    /**
     * Accessor method for list of fantasy teams for combo boxes etc
     */
    public String[] getFTeams(){
        String [] teamList = new String[ draft.getTeams().size()];
        
        for(int i=0;i<teamList.length;i++){
            teamList[i] =  draft.getTeams().get(i).getTeamName();
        }
        
        return teamList;
    }
    /**
     * Accessor method for getting the file manager, which knows how
     * to read and write course data from/to files.
     
    public DraftFileManager getFileManager() {
        return fileManager;
    }
    **/
    
    /**
     * Resets the draft to its default initialized settings, triggering
     * the UI to reflect these changes.
     */
    public void reset() {
        // CLEAR ALL THE COURSE VALUES
//        draft.setSubject(DEFAULT_COURSE_SUBJECT);
//        draft.setNumber(DEFAULT_NUM);
//        draft.setTitle(DEFAULT_TEXT);
//        draft.setSemester(DEFAULT_SEMESTER);
//        draft.setYear(LocalDate.now().getYear());
//        draft.setStartingMonday(nextMonday);
//        draft.setEndingFriday(getNextFriday(nextMonday));
//        draft.clearLectureDays();
//        draft.clearPages();
        
        // AND THEN FORCE THE UI TO RELOAD THE UPDATED COURSE
        view.reloadDraft(draft);
    }
    
    // PRIVATE HELPER METHODS
    
}
