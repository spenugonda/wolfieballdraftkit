package wdk.data;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Team{
    StringProperty TeamName;
    String TeamOwner;
    ObservableList<Player> StartingLineup;
    ObservableList<Player> TaxiSquad;
    //int C=0;
    IntegerProperty playersNeeded=new SimpleIntegerProperty(23);
    int[] positions=new int[10];
    String[] positionsStr = {"C","1B","CI","3B","2B","MI","SS","OF","U","P"};
    int[] positionsLimit = {2,1,1,1,1,1,1,5,1,9};
    
    DoubleProperty moneyLeft = new SimpleDoubleProperty(260);
    DoubleProperty moneyPP = new SimpleDoubleProperty(-1);
    IntegerProperty RTotal = new SimpleIntegerProperty(0);
    IntegerProperty HRTotal = new SimpleIntegerProperty(0);
    IntegerProperty RBITotal = new SimpleIntegerProperty(0);
    IntegerProperty SBTotal = new SimpleIntegerProperty(0);
    DoubleProperty BATotal = new SimpleDoubleProperty(0);
    double totalAtBats=0;
    
    IntegerProperty WTotal = new SimpleIntegerProperty(0);
    IntegerProperty SVTotal = new SimpleIntegerProperty(0);
    IntegerProperty KTotal = new SimpleIntegerProperty(0);
    double totalIP=0;
    DoubleProperty ERATotal = new SimpleDoubleProperty(0);
    DoubleProperty WHIPTotal = new SimpleDoubleProperty(0);
    
    IntegerProperty TotPointsTotal = new SimpleIntegerProperty(0);
    public Team(String teamName,String teamOwner){
        TeamName = new SimpleStringProperty(teamName);
        TeamOwner=teamOwner;
        StartingLineup=FXCollections.observableArrayList();
        TaxiSquad=FXCollections.observableArrayList();
    }
    public String getTeamName(){
        return TeamName.get();
    }
    @Override
    public String toString(){
        return TeamName.get();
    }
    public void addPlayer(Player player){
        if(!isFull()){

            Add(player);
            moneyLeft.set(moneyLeft.get()-player.getsalary());
            
        }
        else if(!isTaxiFull()){
            TaxiSquad.add(player);
        }
        playersNeeded.set(playersNeeded.get()-1);
        int i=-1;
        if(playersNeeded.get()!=0)
            i = (int)moneyLeft.get()/playersNeeded.get();
        moneyPP.set(i);
        this.updateStats();
        
    }
    public boolean isFull(){
        return StartingLineup.size()==23;
    }
    public ObservableList<Player> getSLPlayers(){
        return StartingLineup;
    }        
    public ObservableList<Player> getTaxiPlayers(){
        return TaxiSquad;
    }
    public String getOwnerName() {
        return TeamOwner;
    }
    public void setDetails(String name,String owner){
        TeamName.set(name);
        TeamOwner = owner;
    }
//    public boolean canAdd(Player player) {
//        if(player.qualifiesPosition(C)&&C<=2)
//            
//    }

    private void  Add(Player player) {
        String pos = player.getFTPos();
        if(posIsFull(pos)){
            return;
        }
        int index=0;
        
        
        StartingLineup.add(index, player);
    }
    private int getLast(String pos){
        int index=0;
        int end =0;
        if(pos.equalsIgnoreCase("c")){
            end=0;
        }
        else if(pos.equalsIgnoreCase("1b")){
            end=1;
        }
        else if(pos.equalsIgnoreCase("ci")){
            end=2;
        }
        else if(pos.equalsIgnoreCase("3b")){
            end=3;
        }
        else if(pos.equalsIgnoreCase("2b")){
            end=4;
        }
        else if(pos.equalsIgnoreCase("mi")){
            end=5;
        }
        else if(pos.equalsIgnoreCase("ss")){
            end=6;
        }
        else if(pos.equalsIgnoreCase("of")){
            end=7;
        }
        else if(pos.equalsIgnoreCase("u")){
            end=8;
        }
        else if(pos.equalsIgnoreCase("p")){
            end=9;
        }
        for(int i=0;i<end;i++){
           index+= positions[i];
        }
        return index;
    }

    public boolean posIsFull(String pos) {
        if(pos.equalsIgnoreCase("c")){
            if(positions[0]==2){
                return true;
            }
        }
        if(pos.equalsIgnoreCase("1b")){
            if(positions[1]==1){
                return true;
            }
        }
        if(pos.equalsIgnoreCase("ci")){
            if(positions[2]==1){
                return true;
            }
        }
        if(pos.equalsIgnoreCase("3b")){
            if(positions[3]==1){
                return true;
            }
        }
        if(pos.equalsIgnoreCase("2b")){
            if(positions[4]==1){
                return true;
            }
        }
        if(pos.equalsIgnoreCase("mi")){
            if(positions[5]==1){
                return true;
            }
        }
        if(pos.equalsIgnoreCase("ss")){
            if(positions[6]==1){
                return true;
            }
        }
        if(pos.equalsIgnoreCase("of")){
            if(positions[7]==5){
                return true;
            }
        }
        if(pos.equalsIgnoreCase("u")){
            if(positions[8]==1){
                return true;
            }
        }
        if(pos.equalsIgnoreCase("p")){
            if(positions[9]==9){
                return true;
            }
        }
        
        return false;
    }

    public String getEmptyPostiton() {
        String emptyPos = "";
        for(int i=0;i<positionsLimit.length;i++){
            if(positions[i]<positionsLimit[i]){
                emptyPos = positionsStr[i];
                break;
            }
        }
        return emptyPos;
    }
    public StringProperty teamNameProperty(){
        
        return TeamName;
    }
    public IntegerProperty playersNeededProperty(){
        
        return playersNeeded;
    }
    public DoubleProperty moneyLeftProperty(){
        return moneyLeft;
    }
    public DoubleProperty ppProperty(){
        return moneyPP;
    }
    public IntegerProperty rProperty(){
        return RTotal;
    } 
    public IntegerProperty hrProperty(){
        return HRTotal;
    }
    public IntegerProperty rbiProperty(){
        return RBITotal;
    }
    public IntegerProperty sbProperty(){
        return SBTotal;
    }
    //ba
    public DoubleProperty baProperty(){
        return BATotal;
    }
    //w
    public IntegerProperty wProperty(){
        return WTotal;
    }
    //sv
    public IntegerProperty svProperty(){
        return SVTotal;
    }
    //k
    public IntegerProperty kProperty(){
        return KTotal;
    }
    //era
    public DoubleProperty eraProperty(){
        return ERATotal;
    }
    //whip
    public DoubleProperty whipProperty(){
        return WHIPTotal;
    }
    public IntegerProperty totalPointsProperty(){
        return TotPointsTotal;
    }
    public void updateStats() {
        //initialize the values to zero
        RTotal.set(0);
        HRTotal.set(0);
        RBITotal.set(0);
        SBTotal.set(0);
        BATotal.set(0);
        SVTotal.set(0);
        KTotal.set(0);
        WHIPTotal.set(0);
        ERATotal.set(0);
        WTotal.set(0);
        positions = new int[10];
        playersNeeded.set(23);
        //go over each player
        for(Player player:StartingLineup){
            //if hes not a pitcher, add hitter stats
            if(!player.getFTPos().equalsIgnoreCase("P")){
                RTotal.set(RTotal.get()+player.getstat1());
                HRTotal.set(HRTotal.get()+player.getstat2());
                RBITotal.set(RBITotal.get()+player.getstat3());
                SBTotal.set(SBTotal.get()+(int)player.getstat4());
                //calc total BA
                //multiply total BA with total atbats
                double totalHits = BATotal.get()*totalAtBats;
                //get player's hits and add
                totalHits+=player.getstat5()*player.getAtBats();
                //increment totalAtBats
                totalAtBats+=player.getAtBats();
                //calculate new BATotal
                if(totalAtBats!=0){
                    BATotal.set(totalHits/totalAtBats);
                    
                }
                
                String pos = player.getFTPos();
                //update positions count
                if(pos.equalsIgnoreCase("c")){
                    positions[0]++;
                }
                else if(pos.equalsIgnoreCase("1b")){
                    positions[1]++;
                }
                //either 1b or 3b
                else if(pos.equalsIgnoreCase("ci")){
                    positions[2]++;
                }
                else if(pos.equalsIgnoreCase("3b")){
                    positions[3]++;
                }
                else if(pos.equalsIgnoreCase("2b")){
                    positions[4]++;
                }
                //either a 2b or ss
                else if(pos.equalsIgnoreCase("mi")){
                    positions[5]++;
                }
                else if(pos.equalsIgnoreCase("ss")){
                    positions[6]++;
                }
                else if(pos.equalsIgnoreCase("of")){
                    positions[7]++;
                }
                //any hitting pos
                else if(pos.equalsIgnoreCase("u")){
                    positions[8]++;
                }

            }
            else{
                WTotal.set(WTotal.get()+player.getstat1());
                SVTotal.set(SVTotal.get()+player.getstat2());
                KTotal.set(KTotal.get()+player.getstat3());
                
                positions[9]++;
                //calc ERA
                double totalruns = ERATotal.get()*totalIP;
                totalruns+=player.getstat4()*player.getIP();
                
                //calc WHIP
                double totalhitnwalks = WHIPTotal.get()*totalIP;
                totalhitnwalks+=player.getstat5()*player.getIP();
                
                totalIP+=player.getIP();
                if(totalIP!=0){
                    ERATotal.set(totalruns/totalIP);
                }
                
                if(totalIP!=0){
                    WHIPTotal.set(totalhitnwalks/totalIP);
                }
                
            }    
            playersNeeded.set(playersNeeded.get()-1);
        }
        BATotal.set((double)Math.round(BATotal.get()*1000)/1000);
        ERATotal.set((double)Math.round(ERATotal.get()*100)/100);
        WHIPTotal.set((double)Math.round(WHIPTotal.get()*100)/100);
    }

    public boolean isTaxiFull() {
        return (TaxiSquad.size()==8);
    }

    public void incTotal() {
        TotPointsTotal.set(TotPointsTotal.get()+1);
    }

    public void resetTotalPoints() {
        TotPointsTotal.set(0);
    }
}