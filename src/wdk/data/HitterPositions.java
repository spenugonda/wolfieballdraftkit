package wdk.data;
/**
*A type to represent the types of Hitter positions
*in a team.
*
*@author Sushal Penugonda
**/
public enum HitterPositions{
	c,
	_1B,
	_3B,
	CI,
	_2B,
	SS,
	MI,
	OF,
	U

}