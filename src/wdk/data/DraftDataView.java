package wdk.data;

/**
 *
 * @author Sushal
 */
public interface DraftDataView {
    public void reloadDraft(Draft draftToReload);
}
