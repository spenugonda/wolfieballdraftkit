package wdk.data;

/**
*A type to represent the types of pitcher positions 
*in a team.
*
*@author Sushal Penugonda
**/
public enum PitcherPositions{
	P1,
	P2,
	P3,
	P4,
	P5,
	P6,
	P7,
	P8,
	P9
}