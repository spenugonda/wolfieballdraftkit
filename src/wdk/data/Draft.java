package wdk.data;

import wdk.data.Team;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class Draft{
    private String DraftName;
    private LocalDate LastModified;
    private ObservableList<Player> Players;
    private ObservableList<Team> Teams;
    private ObservableList<Player> MLBPlayers;
    private int tranCount=0;
    public Draft(String draftName){
        DraftName = draftName;
        Players = FXCollections.observableArrayList();
        Teams = FXCollections.observableArrayList();
        MLBPlayers = FXCollections.observableArrayList();
    }
    public String getDraftName(){
        return DraftName;
    }
    public void addPlayer(Player player){
        Players.add(player);
    }
    
    public ObservableList<Player> getPlayers(){
        return Players;
    }
    public ObservableList<Team> getTeams(){
        return Teams;
    }
    public int getCount(){
        tranCount++;
        return tranCount;
    }
    public void incCount(){
        tranCount++;
    }
    public boolean addTeam(String TeamName,String TeamOwner){
        for(Team i:Teams){
            if(i.getTeamName().equalsIgnoreCase(TeamName)){
                return false;
            }
        }
        Teams.add(new Team(TeamName,TeamOwner));
        return true;
    }

    public void removePlayer(Player playerToRemove) {
       Players.remove(playerToRemove);
    }

    public void removeTeam(Team temp) {
        if(temp==null)return;
        //move players to free agent pool
        for(Player i:temp.getSLPlayers()){
            Players.add(i);
            temp.getSLPlayers().remove(i);
        }
        //remove team temp from the list
        for(int i=0;i<Teams.size();i++){
            if(temp.getTeamName().equalsIgnoreCase(Teams.get(i).getTeamName())){
                Teams.remove(i);
                break;
            }
        }
    }
    public void removeTeam(String teamName){
        for(int i=0;i<Teams.size();i++){
            if(Teams.get(i).getTeamName().equalsIgnoreCase(teamName))
            {
                Teams.remove(i);
            }
        }
    }
    public Team getTeam(String TeamName){
        for(Team i:Teams){
            if(i.getTeamName().equalsIgnoreCase(TeamName)){
                return i;
            }
        }
        return null;
    }
    public Player getPlayer(String position){
        Player player = null;
        for(Player i:Players){
            
            if(position.equalsIgnoreCase("CI")){
                if(i.getpositions().contains("1B")||i.getpositions().contains("3B")){
                    player=i;
                    break;
                }
            }
            else if(position.equalsIgnoreCase("MI")){
                if(i.getpositions().contains("2B")||i.getpositions().contains("SS")){
                    player=i;
                    break;
                }
            }
            else if(position.equalsIgnoreCase("U")){
                if(!i.getpositions().contains("p")){
                    player=i;
                    break;
                }
            }
            else if(i.getpositions().contains(position)){
                player = i;
                break;
            }
            
        }
        return player;
    }
    //copies all players to MLB list must be done only once in the beginning
    public void makeMLBList() {
        MLBPlayers.addAll(Players);
    }

    public ObservableList<Player> getMLBPlayers() {
        return MLBPlayers;
    }
}