package wdk;

/**
 * These are properties that are to be loaded from properties.xml. They
 * will provide custom labels and other UI details for our Draft Site Builder
 * application. The reason for doing this is to swap out UI text and icons
 * easily without having to touch our code. It also allows for language
 * independence.
 * 
 * @author Richard McKenna
 */
public enum WDK_PropertyType {
        // LOADED FROM properties.xml
        PROP_APP_TITLE,
        HOME_SCREEN_TITLE,
        PLAYER_SCREEN_TITLE,
        STANDING_SCREEN_TITLE,
        DRAFT_SCREEN_TITLE,
        MLB_SCREEN_TITLE,
        
        // APPLICATION ICONS
        NEW_DRAFT_ICON,
        LOAD_DRAFT_ICON,
        SAVE_DRAFT_ICON,
    //VIEW_SCHEDULE_ICON,
        EXPORT_PAGE_ICON,
        //DELETE_ICON,
        EXIT_ICON,
        
		FANTASY_TEAMS_TAB_ICON,
		PLAYERS_TAB_ICON,
		FANTASY_STANDINGS_TAB_ICON,
		DRAFT_TAB_ICON,
		MLB_TEAMS_TAB_ICON,
		
		ADD_ICON,
        MINUS_ICON,
        EDIT_ICON,
        //MOVE_UP_ICON,
        //MOVE_DOWN_ICON,
        
        // APPLICATION TOOLTIPS FOR BUTTONS
        NEW_DRAFT_TOOLTIP,
        LOAD_DRAFT_TOOLTIP,
        SAVE_DRAFT_TOOLTIP,
        //VIEW_SCHEDULE_TOOLTIP,
        EXPORT_PAGE_TOOLTIP,
        FANTASY_TEAMS_TOOLTIP,
        PLAYERS_TOOLTIP,
        FANTASY_STANDINGS_TOOLTIP,
        DRAFT_TOOLTIP,
        MLB_TEAMS_TOOLTIP,
        EDIT_DRAFT_TOOLTIP,
        //DELETE_TOOLTIP,
        EXIT_TOOLTIP,
        
        //RADIO BUTTONS
        ALL_RB_TEXT,
        C_RB_TEXT,
        _1B_RB_TEXT,
        CI_RB_TEXT,
        _3B_RB_TEXT,
        _2B_RB_TEXT,
        MI_RB_TEXT,
        SS_RB_TEXT,
        OF_RB_TEXT,
        U_RB_TEXT,
        P_RB_TEXT,
        
        ADD_ITEM_TOOLTIP,
        REMOVE_ITEM_TOOLTIP,
			//ADD_LECTURE_TOOLTIP,
			//REMOVE_LECTURE_TOOLTIP,
			//MOVE_UP_LECTURE_TOOLTIP,
			//MOVE_DOWN_LECTURE_TOOLTIP,
			//ADD_HW_TOOLTIP,
			//REMOVE_HW_TOOLTIP,        

        // FOR DRAFT EDIT WORKSPACE
        PLAYERS_HEADING_LABEL,
        FANTASY_TEAMS_HEADING_LABEL,
        FANTASY_STANDINGS_HEADING_LABEL,
        DRAFT_HEADING_LABEL,
        MLB_TEAMS_HEADING_LABEL,
        SEARCH_BAR_LABEL,
	DRAFT_NAME_LABEL,
        // ERROR DIALOG MESSAGES
        
        
        // AND VERIFICATION MESSAGES
        NEW_DRAFT_CREATED_MESSAGE,
        DRAFT_LOADED_MESSAGE,
        DRAFT_SAVED_MESSAGE,
        SITE_EXPORTED_MESSAGE,
        SAVE_UNSAVED_WORK_MESSAGE,
        REMOVE_ITEM_MESSAGE
}
